Feichero
Jednoduchá 2D hra

Úvod:
Feichero je desktopová arcade hra na principu mobilní hry Archero. Hráč v ní může manipulovat s postavou lukostřelce, které střílí v určitých intervalech šípy na nejbližšího protivníka. Postava může šípy střílet pouze pokud se nehýbe.

Povinné funkcionality
1.	Načítání herního levelu z XML souboru. Následný přechod mezi herními levely. (Realizováno, nutno pouze ošetřit výjimky, které mohou nastat při nekorektním/chybně zapsaném XML souboru).
2.	4 druhy nepřátel:
a.	Typ 1: Přibližuje se konstantní rychlostí k hráči a útočí na blízko v určitých intervalech. (Realizováno)
b.	Typ 2: Pohybuje se po herní ploše, přibližuje se k hráči pouze pokud je k němu blízko. V náhodných intervalech se ale k hráči nebezpečně blízko teleportuje. Teleportace má prodlevu (Napsán kód, nutno otestovat)
c.	Typ 3: Nepohybuje se a v určitých intervalech střílí po hráči střeli. (Realizováno)
d.	Typ 4: Přibližuje se k hráči stejně jako Typ 1. V určitých intervalech zvyšuje rychlost (Realizováno)
3.	Útok na dálku hráčů a nepřátel. (Realizovány dva druhy: Útoky, které se neprovedou, protože v cestě stojí překážka a vzdušné které překážky ignorují)
4.	Hledání nejkratší cesty mezi dvěma objekty. Typy nepřátel, které mají pouze útok na blízko musí najít k hráči nejkratší cestu. (Realizováno Dijkstrovým algoritmem)
5.	Neprůchozí objekty (Realizováno)
6.	Ovládání hráče pomocí kláves (Realizováno)
7.	Plynulé animace a pohyby objektů (Realizováno s bugy)
8.	Herní menu (Částečně realizováno)

Volitelná rozšíření (nice to have):
1.	Optimalizace hledání nejkratší cesty mezi dvěma objekty. Hra využívá Dijkstrův algoritmus pro hledání nejkratší trasy mezi dvěma objekty v herním levelu. Přidání cashování nebo optimalizace této funkcionality, aby se dosáhlo lepšího výkonu hry: (Částečně implementováno cashování ohodnocených cest, nicméně nefunguje kvůli bugu)
2.	Generování herních map. (Částečně implementováno pouze pro generování prvních testovacích map, které byli následně upraveny pro potřeby testování nových funkcionalit)
3.	Další druhy nepřátel:
a.	Typ 4: Nepřítel, který se při vyčerpání všech životů rozpadne na 3 další druhy nepřátel.
b.	Typ 5: Sebevražedný nepřítel, který má obrovské množství životů, přibližuje se pomalou rychlostí k hráčovi. Pokud se přiblíží dostatečně blízko vybuchne a způsobí větší poškození (Minecraft Creeper).
c.	Typ 6: Nepřítel, co je chvílemi neviditelný.
d.	Typ 7: Nepřítel se v při poklesu životů pod určitou hladinu znásobí.
4.	Dočasná vylepšení v průběhu hry: Větší poškození, více životů, vyšší rychlost.

