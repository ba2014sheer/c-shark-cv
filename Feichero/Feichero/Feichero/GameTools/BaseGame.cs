﻿using System;
using System.Collections.Generic;
using Feichero.Game.Character;
using Feichero.Game.FileWorker;
using Feichero.Gui;

namespace Feichero.Game
{
    /// <summary>
    /// Statická třída která vytváří a napojuje jednotlivé prvky hry
    /// </summary>
    public static class BaseGame
    {
        /// <summary>
        /// Počáteční šířka postavy
        /// </summary>
        public static readonly int CharacterWidth = 50;

        /// <summary>
        /// Počáteční velikost postavy
        /// </summary>
        public static readonly int CharacterHeight = 50;

        /// <summary>
        /// Definuje výchozí rozměry levelu tak aby se vešel do herního okna
        /// </summary>
        public static readonly int DefaultPlaygroundWidth = 15;

        /// <summary>
        /// Definuje výchozí rozměry levelu tak aby se vešel do herního okna
        /// </summary>
        public static readonly int DefaultPlaygroundHeight = 11;

        /// <summary>
        /// Výchozí hodnota životu
        /// </summary>
        public static readonly int DefaultPlayerLife = 200;

        /// <summary>
        /// Převede enum nepřítele na text
        /// </summary>
        /// <param name="kind">enum nepřítele</param>
        /// <returns>Textové vyjádření</returns>
        public static string EnemyToText(EnemyKind kind)
        {
            switch (kind)
            {
                case EnemyKind.MELTED_CARLO:
                    return "Melted Carlo";
                case EnemyKind.CURSED_ORACLE:
                    return "Cursed Oracle";
                case EnemyKind.LE_EXCEPTION:
                    return "Le Exception";
                case EnemyKind.SEGMENT_THE_FAULTED:
                    return "Segment the Faulted";
                default:
                    return null;
            }
        }

        /// <summary>
        /// Vytvoří nepřítele
        /// </summary>
        /// <param name="kind">Druh nepřítele</param>
        /// <param name="x">Počáteční Xová souřadnice</param>
        /// <param name="y">Počáteční Yová souřadnice</param>
        /// <param name="levelCharacters"></param>
        /// <returns>Nepřítel</returns>
        public static Enemy CreateEnemy(EnemyKind kind, int x, int y, Level level)
        {
            Enemy enemy;
            switch (kind)
            {
                case EnemyKind.MELTED_CARLO:
                    enemy = new MeltedCarlo(kind, x, y, CharacterWidth, CharacterHeight, 500, Attack.MELEE);
                    break;
                case EnemyKind.CURSED_ORACLE:
                    enemy = new CursedOracle(kind, x, y, CharacterWidth, CharacterHeight, 300, Attack.MELEE);
                    break;
                case EnemyKind.LE_EXCEPTION:
                    enemy = new LeException(kind, x, y, CharacterWidth, CharacterHeight, 400, Attack.RANGE);
                    break;
                case EnemyKind.SEGMENT_THE_FAULTED:
                    enemy = new SegmentTheFaulted(kind, x, y, CharacterWidth, CharacterHeight, 150, Attack.MELEE);
                    break;
                default:
                    return null;
            }

            CreateEnemy(enemy, level);
            return enemy;
        }
        
        /// <summary>
        /// Přidá nepřítele na mapu
        /// </summary>
        /// <param name="enemy">Nepřítel</param>
        /// <param name="level">mapa</param>
        public static void CreateEnemy(Enemy enemy, Level level) {
            enemy.Level = level;
            enemy.Blocks = level.Blocks;
            enemy.ObjectDelete += delegate
            {
                level.CharacterSemaphore.WaitOne();
                level.Characters.Remove(enemy);
                level.CharacterSemaphore.Release();
            };
            level.Characters.Add(enemy);
        }

        /// <summary>
        /// Detekuje kolizi dvou objektu
        /// </summary>
        /// <param name="o1">Objekt 1</param>
        /// <param name="o2">Objekt 2</param>
        /// <param name="delta">odchylka</param>
        /// <returns>true - kolize, false - nedoslo ke kolizi</returns>
        public static bool ColisionCheckDelta(IObject o1, IObject o2, double delta)
        {
            double WidthO1 = o1.Width - delta;
            double WidthO2 = o2.Width - delta;
            double HeightO1 = o1.Height - delta;
            double HeightO2 = o2.Height - delta;
            double X01 = o1.X + delta;
            double X02 = o2.X + delta;
            double Y01 = o1.Y + delta;
            double Y02 = o2.Y + delta;
            
            if (X01 < X02 + WidthO2 &&
                X01 + WidthO1 > X02 &&
                Y01 < Y02 + HeightO2 &&
                Y01 + HeightO1 > Y02)
                return true;
            return false;
        }

        /// <summary>
        /// Detekuje kolizi dvou objektu
        /// </summary>
        /// <param name="o1">Objekt 1</param>
        /// <param name="o2">Objekt 2</param>
        /// <returns>true - kolize, false - nedoslo ke kolizi</returns>
        public static bool CollisionCheck(IObject o1, IObject o2)
        {
            if (o1.X < o2.X + o2.Width &&
                o1.X + o1.Width > o2.X &&
                o1.Y < o2.Y + o2.Height &&
                o1.Y + o1.Height > o2.Y)
                return true;
            return false;
        }

        /// <summary>
        /// Detekuje kolizi ještě nezaloženého objektu
        /// </summary>
        /// <param name="x">Xová souřadnice objektu</param>
        /// <param name="y">Yová souřadnice objektu</param>
        /// <param name="width">Šířka</param>
        /// <param name="height">Výška</param>
        /// <param name="o2">Druhý objekt</param>
        /// <returns></returns>
        public static bool CollisionCheck(int x, int y, int width, int height, IObject o2)
        {
            if (x < o2.X + o2.Width &&
                x + width > o2.X &&
                y < o2.Y + o2.Height &&
                y + height > o2.Y)
                return true;
            return false;
        }
        
        /// <summary>
        /// Vypocita procentualni kolizi dvou objektu
        /// </summary>
        /// <param name="o1">objekt 1</param>
        /// <param name="o2">objekt 2</param>
        /// <returns>Kolize v procentech</returns>
        public static double CollisionOverlap(IObject o1, IObject o2)
        {
            var l0 = o1.X;
            var r0 = o1.X + o1.Width;
            var t0 = o1.Y;
            var b0 = o1.Y + o1.Height;
            
            var l1 = o2.X;
            var r1 = o2.X + o2.Width;
            var t1 = o2.Y;
            var b1 = o2.Y + o2.Height;
            
            var a0 = o1.Width * o1.Height;
            var a1 = o2.Width * o2.Height;
            
            var overlap = (Math.Max(l0, l1) - Math.Min(r0, r1)) * (Math.Max(t0, t1) - Math.Min(b0, b1));
            
            return  (overlap)/(a0 + a1 - overlap);
        }

        /// <summary>
        /// Vygeneruje levely
        /// </summary>
        /// <returns>Generované levely</returns>
        public static Queue<Level> GenerateRandomLevels(string outPutPath, int count)
        {
            Queue<Level> queue = new Queue<Level>();
            InstanceSaver saver = new InstanceSaver();

            for (int i = 1; i < count + 1; i++)
            {
                Level level = new Level(DefaultPlaygroundWidth, DefaultPlaygroundHeight);
                level.LoadedHash = i;
                GenerateEnemies(i, level);
                level.Player = new Player($"Random - {i}", 500, 500, Components.BlockSize, Components.BlockSize, DefaultPlayerLife);

                string path = $"{outPutPath}/random{level.GetHashCode()}.fexml";
                saver.SaveLevel(level, path);
                queue.Enqueue(saver.Deserialize(path));
            }

            return queue;
        }

        /// <summary>
        /// Vygeneruje nepřátele
        /// </summary>
        /// <param name="i">obtížnost</param>
        /// <param name="level">Level pro přidání nepřítele</param>
        private static void GenerateEnemies(int i, Level level)
        {
            Random rd = new Random();

            for (int j = i; j < (i + 1); j++)
            {
                switch (rd.Next(0, Enemy.Count))
                {
                    case 0:
                        level.Add(CreateEnemy(EnemyKind.CURSED_ORACLE, j * Components.BlockSize,
                            j * Components.BlockSize, level));
                        break;
                    case 1:
                        level.Add(CreateEnemy(EnemyKind.MELTED_CARLO, j * Components.BlockSize,
                            j * Components.BlockSize, level));
                        break;
                    case 2:
                        level.Add(CreateEnemy(EnemyKind.LE_EXCEPTION, j * Components.BlockSize,
                            j * Components.BlockSize, level));
                        break;
                    case 3:
                        level.Add(CreateEnemy(EnemyKind.SEGMENT_THE_FAULTED, j * Components.BlockSize,
                            j * Components.BlockSize, level));
                        break;
                }
            }
        }

        /// <summary>
        /// Zkontroluje jestli dráha střeli protne překážku
        /// </summary>
        /// <param name="target">Nepřítel</param>
        /// <param name="block">Překážka</param>
        /// <returns>true - protne / false - neprotne</returns>
        public static bool IsShotable(ICharacter character, ICharacter target, IObject block)
        {
            int x1 = (int) character.X;
            int y1 = (int) character.Y;
            int x2 = (int) target.X;
            int y2 = (int) target.Y;
            int rx = (int) block.X;
            int ry = (int) block.Y;
            var rw = block.Width;
            var rh = block.Height;

            bool top = CrossShootBlock(x1, y1, x2, y2, rx, ry, rx + rw, ry);
            bool bottom = CrossShootBlock(x1, y1, x2, y2, rx, ry + rh, rx + rw, ry + rh);
            bool left = CrossShootBlock(x1, y1, x2, y2, rx, ry, rx, ry + rh);
            bool right = CrossShootBlock(x1, y1, x2, y2, rx + rw, ry, rx + rw, ry + rh);

            if (left || right || top || bottom)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Zkontroluje jestli přímka protne stranu překážky
        /// </summary>
        /// <param name="x1">Počáteční bod X</param>
        /// <param name="y1">Počáteční bod Y</param>
        /// <param name="x2">Koncový bod X</param>
        /// <param name="y2">Koncový bod Y</param>
        /// <param name="x3">Počáteční bod X překážky</param>
        /// <param name="y3">Počáteční bod Y překážky</param>
        /// <param name="x4">Koncový bod X překážky</param>
        /// <param name="y4">Koncový bod Y< překážky/param>
        /// <returns></returns>
        private static bool CrossShootBlock(float x1, float y1, float x2, float y2, float x3, float y3, float x4,
            float y4)
        {
            float uA = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) /
                       ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));
            float uB = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) /
                       ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));
            if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Algorithm for incrementing values of bullet
        /// </summary>
        /// <param name="x">Bullet location X</param>
        /// <param name="y">Bullet location Y</param>
        /// <param name="x2">Bullet target X</param>
        /// <param name="y2">Bullet target Y</param>
        /// <param name="nX">Next bullet location X</param>
        /// <param name="nY">Next bullet location Y</param>
        public static void BLineNext(int x, int y, int x2, int y2, out int nX, out int nY)
        {
            nX = nY = 0;
            int w = x2 - x;
            int h = y2 - y;
            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1;
            else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1;
            else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1;
            else if (w > 0) dx2 = 1;
            int longest = Math.Abs(w);
            int shortest = Math.Abs(h);
            if (!(longest > shortest)) {
                longest = Math.Abs(h);
                shortest = Math.Abs(w);
                if (h < 0) dy2 = -1;
                else if (h > 0) dy2 = 1;
                dx2 = 0;
            }

            int numerator = longest >> 1;
            numerator += shortest;
            if (!(numerator < longest)) {
                x += dx1;
                y += dy1;
            }
            else {
                x += dx2;
                y += dy2;
            }

            nX = x;
            nY = y;
        }
        
        /// <summary>
        /// Vypocita vzdalenost svou objektu
        /// </summary>
        /// <param name="o1">objekt 1</param>
        /// <param name="o2">objekt 2</param>
        /// <returns>vzdalenost</returns>
        public static double Distance(IObject o1, IObject o2)
        {
            return Distance((int) o1.X, (int) o1.Y, (int) o2.X, (int) o2.Y);
        } 
        
        /// <summary>
        /// Vypocita vzdalenost svou objektu
        /// </summary>
        /// <param name="x1">objekt 1 X</param>
        /// <param name="y1">objekt 1 Y</param>
        /// <param name="x2">objekt 2 X</param>
        /// <param name="y2">objekt 2 Y</param>
        /// <returns>vzdalenost</returns>
        public static double Distance(int x1, int y1, int x2, int y2) {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) +  
                             Math.Pow(y2 - y1, 2) * 1.0); 
        } 
    }
}