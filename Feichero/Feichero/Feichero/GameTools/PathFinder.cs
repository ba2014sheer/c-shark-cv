﻿using System;
using System.Collections.Generic;
using Feichero.Gui;

namespace Feichero {
    namespace Game {
        /// <summary>
        /// Třída poskytující pro level hledání jekratší cesty. Využívá Dijkstrův algoritmus.
        /// </summary>
        public class PathFinder {
            /// <summary>
            /// Počet řádků levelu
            /// </summary>
            public int ROW { get;}
            /// <summary>
            /// Počet sloupců levelu
            /// </summary>
            public int COL { get;}
            /// <summary>
            /// Zacachevoné záznamy. Pokud už jednou byla trasa nalezena je zbytečné ji znovu hledat.
            /// </summary>
            private Dictionary<Tuple<Point, Point>, PricePath[,]> PathCache = new Dictionary<Tuple<Point, Point>, PricePath[,]>();
            /// <summary>
            /// Bod v bludišti
            /// </summary>
            public class Point
            {
                /// <summary>
                /// Xová souřadnice
                /// </summary>
                public int X { get;}
                /// <summary>
                /// Yová souřadnice
                /// </summary>
                public int Y { get;}
                /// <summary>
                /// Konstruktor
                /// </summary>
                /// <param name="x">Xová souřadnice</param>
                /// <param name="y">Yová souřadnice</param>
                public Point(int x, int y)
                {
                    X = x;
                    Y = y;
                }

                public override bool Equals(object obj)
                {
                    return obj != null && obj.GetType() == typeof(Point) && ((X == ((Point)obj).X) && (Y == ((Point)obj).Y));
                }

                public override int GetHashCode()
                {
                    return X * 100 + Y + 100;
                }
            }

            /// <summary>
            /// Ohodnocená cesta
            /// </summary>
            public class PricePath
            {
                /// <summary>
                /// Bod, který byl ohodnocen
                /// </summary>
                public Point Pt { get;}
                /// <summary>
                /// Distance nebo-li ohodnocení bodu
                /// </summary>
                public int Dist { get;}

                /// <summary>
                /// Konstruktor
                /// </summary>
                /// <param name="pt">Bod, který byl ohodnocen</param>
                /// <param name="dist">Distance nebo-li ohodnocení bodu</param>
                public PricePath(Point pt, int dist)
                {
                    Pt = pt;
                    Dist = dist;
                }
            }

            
            
            /// <summary>
            /// Konstruktor
            /// </summary>
            /// <param name="row">Počet řádků levelu</param>
            /// <param name="col">Počet sloupců levelu</param>
            public PathFinder(int row, int col)
            {
                ROW = row + 1;
                COL = col + 1;
            }

            /// <summary>
            /// Kontrola validního bodu
            /// </summary>
            /// <param name="row">Řádek</param>
            /// <param name="col">Sloupec</param>
            /// <returns>true - validní false - nevalidní</returns>
            public bool isValid(int row, int col)
            {
                return (row >= 0) && (row < ROW) &&
                       (col >= 0) && (col < COL);
            }

            private int[] rowNum = {-1, 0, 0, 1};
            private int[] colNum = {0, -1, 1, 0};

            /// <summary>
            /// Najde nejkratší cestu na herní ploše
            /// </summary>
            /// <param name="playground">Herní plocha kde každé políčko null je volná cesta, obsazené objektem IObject jsou překážky</param>
            /// <param name="start">Počítek</param>
            /// <param name="target">Cíl</param>
            /// <returns>Ohodnocené trasy k cíli</returns>
            public PricePath[,] FindMeAPath(IObject[,] playground, IObject start, IObject target)
            {
                Point dest = new Point((int) (start.X / Components.BlockSize), (int) (start.Y / Components.BlockSize));
                Point src = new Point((int) (target.X / Components.BlockSize), (int) (target.Y / Components.BlockSize));
                
                foreach (var pathCacheKey in PathCache.Keys)
                    if (pathCacheKey.Item1.Equals(dest) && pathCacheKey.Item2.Equals(src))
                        return PathCache[pathCacheKey];
                
                PricePath[,] Path = new PricePath[ROW+1,COL+1];
                bool[,] visited = new bool[ROW, COL];

                visited[src.X, src.Y] = true;

                Queue<PricePath> q = new Queue<PricePath>();
                PricePath s = new PricePath(src, 0);
                q.Enqueue(s);
                Path[src.X, src.Y] = s;
                while (q.Count != 0)
                {
                    PricePath curr = q.Peek();
                    Point pt = curr.Pt;
                    if (pt.X == dest.X && pt.Y == dest.Y)
                    {
                        PathCache.Add(new Tuple<Point, Point>(dest, src), Path);
                        return Path;
                    }
                    q.Dequeue();

                    for (int i = 0; i < 4; i++)
                    {
                        int row = pt.X + rowNum[i];
                        int col = pt.Y + colNum[i];
                        if (isValid(row, col) && playground[row, col] == null && !visited[row, col]) //!
                        {
                            visited[row, col] = true;
                            PricePath Adjcell = new PricePath(new Point(row, col), curr.Dist + 1);
                            q.Enqueue(Adjcell);
                            Path[row, col] = Adjcell;
                        }
                    }
                }
                return null;
            }
        }
    }
}