﻿﻿﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Feichero.Game;
using Feichero.Game.Character;

  namespace Feichero
{
    namespace Gui {
        /// <summary>
        /// Třída pro grafické vykreslení komponent
        /// </summary>
        public static class Components
        {
            /// <summary>
            /// Výchozí velikost jednoho bloku v pixelech herního světa
            /// </summary>
            public static readonly int BlockSize = 50;
            /// <summary>
            /// Health bar odstup od postavy
            /// </summary>
            public static readonly int HealthBarMargin = -20;
            /// <summary>
            /// Výška Health baru
            /// </summary>
            public static readonly int HealthBarHeight = 10;
            /// <summary>
            /// Textury pro vykreslení
            /// </summary>
            private static Dictionary<string, ImageBrush> _textures = new Dictionary<string, ImageBrush>();
            /// <summary>
            /// Statický konstruktor pro naplnění textur
            /// </summary>
            static Components() {
                ImageBrush brush;
                BitmapImage image;
                //Nepřátelé
                brush = new ImageBrush();
                image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri("./Textures/cursedoracle.png", UriKind.Relative);
                image.EndInit();
                brush.ImageSource = image;
                _textures.Add("cursedoracle", brush);
                brush = new ImageBrush();
                image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri("./Textures/leexception.png", UriKind.Relative);
                image.EndInit();
                brush.ImageSource = image;
                _textures.Add("leexception", brush);
                brush = new ImageBrush();
                image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri("./Textures/meltedcarlo.png", UriKind.Relative);
                image.EndInit();
                brush.ImageSource = image;
                _textures.Add("meltedcarlo", brush);
                brush = new ImageBrush();
                image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri("./Textures/segmentthefaulted.png", UriKind.Relative);
                image.EndInit();
                brush.ImageSource = image;
                _textures.Add("segmentthefaulted", brush);
                //Hráč
                brush = new ImageBrush();
                image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri("./Textures/feichero.png", UriKind.Relative);
                image.EndInit();
                brush.ImageSource = image;
                _textures.Add("player", brush);
                //Bloky
                brush = new ImageBrush();
                image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri("./Textures/box.png", UriKind.Relative);
                image.EndInit();
                brush.ImageSource = image;
                _textures.Add("box", brush);
                //Background
                brush = new ImageBrush();
                image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri("./Textures/background.png", UriKind.Relative);
                image.EndInit();
                brush.ImageSource = image;
                brush.TileMode = TileMode.FlipY;
                brush.Stretch = Stretch.Uniform;
                brush.AlignmentY = AlignmentY.Top;
                brush.ViewportUnits = BrushMappingMode.Absolute;
                brush.Viewport = new Rect(0,0,10,10);
                _textures.Add("background", brush);
            }

            /// <summary>
            /// Vloží hráče na herní plochu
            /// </summary>
            /// <param name="player">Hráč</param>
            /// <param name="gameCanvas"></param>
            public static Shape DrawPlayer(Player player, Canvas gameCanvas) {
                Ellipse e = new Ellipse();
                SolidColorBrush mySolidColorBrush = new SolidColorBrush();
                mySolidColorBrush.Color = Color.FromArgb(255, 255, 255, 0);
                e.Fill = _textures["player"];
                //e.Fill = mySolidColorBrush;
                e.Width = player.Width;
                e.Height = player.Height;

                Canvas.SetTop(e, player.X);
                Canvas.SetLeft(e, player.Y);
                
                //Health bar
                Rectangle h = new Rectangle();
                
                //Nastaveni udalosti při pohybu
                player.ObjectMoved += delegate(int x, int y, int pX, int pY) {
                    Canvas.SetLeft(e, x);
                    Canvas.SetTop(e, y);
                    Canvas.SetLeft(h, x);
                    Canvas.SetTop(h,  y+ Components.HealthBarMargin);
                };
                
                LinearGradientBrush gradient = new LinearGradientBrush(
                    Colors.Lime, Colors.Red, 
                    new Point(0, 0), new Point(25, HealthBarHeight)
                );

                h.Width = player.Width;
                h.Height = HealthBarHeight;
                h.Fill = gradient;
                Canvas.SetTop(h, player.Y + Components.HealthBarMargin);
                Canvas.SetLeft(h, player.X);

                player.LifeChange += delegate(int current, int previous, int max)
                {
                    var life = CalculateHealthBarWidth(current, max, player.Width);
                    if(life > 0)
                        h.Width = life;
                };

                player.ObjectDelete += () =>
                {
                    gameCanvas.Children.Remove(e);
                    gameCanvas.Children.Remove(h);
                };

                gameCanvas.Children.Add(e);
                gameCanvas.Children.Add(h);
                return e;
            }
            /// <summary>
            /// Vypočítá aktuální šířku health baru
            /// </summary>
            /// <param name="current">Aktuální</param>
            /// <param name="max">Maximální</param>
            /// <returns>Součásná řířka</returns>
            private static int CalculateHealthBarWidth(int current, int max, int maxHealth)
            {
                return (maxHealth * current) / max;
            }

            /// <summary>
            /// Vykreslí herní postavu podle druhu nepřítele
            /// </summary>
            /// <param name="enemy">Nepřítel</param>
            /// <param name="gameCanvas">Herní canvas</param>
            /// <returns>Shape nepřítele</returns>
            public static Shape DrawEnemy(Enemy enemy, Canvas gameCanvas) {
                Ellipse e = new Ellipse();
                SolidColorBrush mySolidColorBrush = new SolidColorBrush();
                
                switch (enemy.Kind)
                {
                    case EnemyKind.MELTED_CARLO:
                        mySolidColorBrush.Color = Color.FromArgb(255, 0, 255, 0);
                        e.Fill = _textures["meltedcarlo"];
                        break;
                    case EnemyKind.CURSED_ORACLE:
                        mySolidColorBrush.Color = Color.FromArgb(255, 0, 0, 0);
                        e.Fill = _textures["cursedoracle"];
                        break;
                    case EnemyKind.LE_EXCEPTION:
                        mySolidColorBrush.Color = Color.FromArgb(255, 0, 255, 255);
                        e.Fill = _textures["leexception"];
                        break;
                    case EnemyKind.SEGMENT_THE_FAULTED:
                        mySolidColorBrush.Color = Color.FromArgb(255, 255, 125, 255);
                        e.Fill = _textures["segmentthefaulted"];
                        break;
                    default:
                        mySolidColorBrush.Color = Color.FromArgb(0, 0, 0, 0);
                        break;
                }

                
                e.Width = enemy.Width;
                e.Height = enemy.Height;
                
                Canvas.SetTop(e, enemy.Y);
                Canvas.SetLeft(e, enemy.X);
                
                //Health bar
                Rectangle h = new Rectangle();
                LinearGradientBrush gradient = new LinearGradientBrush(
                    Colors.Lime, Colors.Red, 
                    new Point(0, 0), new Point(25, HealthBarHeight)
                );

                h.Width = enemy.Width;
                h.Height = HealthBarHeight;
                h.Fill = gradient;
                Canvas.SetTop(h, enemy.Y + Components.HealthBarMargin);
                Canvas.SetLeft(h, enemy.X);
                

                enemy.ObjectMoved += delegate(int x, int y, int pX, int pY)
                {
                    Canvas.SetTop(h, y + Components.HealthBarMargin);
                    Canvas.SetLeft(h, x);
                    Canvas.SetLeft(e, x);
                    Canvas.SetTop(e, y);
                };
                
                enemy.LifeChange += delegate(int current, int previous, int max)
                {
                    var health = CalculateHealthBarWidth(current, max, enemy.Width);
                    if(health >= 0)
                        h.Width = health;
                };

                enemy.ObjectDelete += () =>
                {
                    gameCanvas.Children.Remove(h);
                    gameCanvas.Children.Remove(e);
                };

                gameCanvas.Children.Add(h);
                gameCanvas.Children.Add(e);
                return e;
            }

            public static Shape DrawBullet(MovingObject o, Canvas gameCanvas)
            {
                Ellipse e = new Ellipse();
                SolidColorBrush mySolidColorBrush = new SolidColorBrush();
                mySolidColorBrush.Color = Color.FromArgb(255, 0, 0, 0);

                e.Fill = mySolidColorBrush;
                e.Width = o.Width;
                e.Height = o.Height;
             
                Canvas.SetTop(e, o.Y);
                Canvas.SetLeft(e, o.X);

                o.ObjectMoved += (x, y, xP, yP) =>
                {
                    Canvas.SetTop(e, y);
                    Canvas.SetLeft(e, x);
                };

                o.ObjectDelete += () =>
                {
                    gameCanvas.Children.Remove(e);
                };

                gameCanvas.Children.Add(e);

                return e;
            }

            /// <summary>
            /// Vykreslí překážku
            /// </summary>
            /// <param name="block">Blok k vykreslení</param>
            /// <returns>Shape bloku</returns>
            public static Shape DrawBlock(IObject block) {
                Rectangle r = new Rectangle();
                SolidColorBrush mySolidColorBrush = new SolidColorBrush();
                mySolidColorBrush.Color = Color.FromArgb(128, 128, 128, 0);
                r.Fill = _textures["box"];
                //r.Fill = mySolidColorBrush;
                r.Width = block.Width;
                r.Height = block.Height;

                Canvas.SetTop(r, block.Y);
                Canvas.SetLeft(r, block.X);

                return r;
            }

            /// <summary>
            /// Vykreslí základní kostu levelu
            /// </summary>
            /// <param name="level">Level k vykreslení</param>
            /// <returns>Vykreslené objekty</returns>
            public static List<Shape> DrawLevel(Level level, Canvas gameCanvas)
            {
                gameCanvas.Background = _textures["background"];
                
                level.ObjectCreaded += delegate(IObject o) {
                    if (o.GetType() == typeof(Bullet))
                        DrawBullet((Bullet)o, gameCanvas);
                };
                
                List<Shape> shapes = new List<Shape>();

                foreach (var block in level.Blocks) {
                    Shape shape = DrawBlock(block);
                    shapes.Add(shape);
                    gameCanvas.Children.Add(shape);
                }
                return shapes;
            }

            public static void DrawEnemy(ICharacter levelACharacter, Canvas gameCanvas)
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Třída pro ovládání postav.
        /// </summary>
        public static class Controls
        {
            /// <summary>
            /// Převede stiknutou klávesu do herního směru
            /// </summary>
            /// <param name="key">Stisknutá klávesa</param>
            /// <returns>Herní směr</returns>
            public static Direction GetDirectionByKey(Key key) {
                Direction direction = Direction.NONE;
                switch (key){
                    case Key.Up:
                    case Key.W:
                        direction = Direction.UP;
                        break;
                    case Key.Down:
                    case Key.S:
                        direction = Direction.DOWN;
                        break;
                    case Key.Left:
                    case Key.A:
                        direction = Direction.LEFT;
                        break;
                    case Key.Right:
                    case Key.D:
                        direction = Direction.RIGHT;
                        break;
                }
                return direction;
            }
        }
    }
}