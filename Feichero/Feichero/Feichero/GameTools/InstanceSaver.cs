﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Feichero.Game.Character;

namespace Feichero
{
    namespace Game
    {
        namespace FileWorker
        {
            /// <summary>
            /// Třída pro zálohování a obnovování herních instancí
            /// </summary>
            public class InstanceSaver
            {
                /// <summary>
                /// Třída pro porovnání levelů
                /// </summary>
                public class LevelComparer : IEqualityComparer<Level>
                {
                    public bool Equals(Level x, Level y)
                    {
                        return x != null && x.Equals(y);
                    }

                    public int GetHashCode(Level obj)
                    {
                        return obj.LoadedHash;
                    }
                }

                /// <summary>
                /// Třída pro serializaci levelu
                /// </summary>
                public class LevelPojo
                {
                    /// <summary>
                    /// Bloky k serializaci
                    /// </summary>
                    public Block[] Blocks;
                    /// <summary>
                    /// Player ke serializaci
                    /// </summary>
                    public Player Player;
                    /// <summary>
                    /// Nepřátelé k serializaci
                    /// </summary>
                    public Enemy[] Enemies;
                    /// <summary>
                    /// Sirka levelu pro Serializaci
                    /// </summary>
                    public int Width;
                    /// <summary>
                    /// Vyska levelu pro serializaci
                    /// </summary>
                    public int Height;
                    /// <summary>
                    /// Defaultní konstruktor pro XML serializaci
                    /// </summary>
                    public int Hash;
                    public LevelPojo()
                    {
                        Hash = GetHashCode();
                    }
                }

                /// <summary>
                /// Serializuje do XML herní blok
                /// </summary>
                /// <param name="level">level k uložení</param>
                /// <param name="path">Cesta k souboru</param>
                /// <returns>Textová serializace v XML</returns>
                public void SaveLevel(Level level, string path)
                {
                    StreamWriter writer = new StreamWriter(path);
                    XmlSerializer serializer = new XmlSerializer(typeof(LevelPojo));

                    LevelPojo pojo = new LevelPojo();
                    pojo.Width = level.Width;
                    pojo.Height = level.Height;
                    
                    pojo.Blocks = new Block[level.Blocks.Count];
                    for (int i = 0; i < level.Blocks.Count; i++)
                        pojo.Blocks[i] = (Block) level.Blocks[i];

                    pojo.Enemies = new Enemy[level.Characters.Count];
                    for (int i = 0; i < level.Characters.Count; i++)
                        pojo.Enemies[i] = (Enemy) level.Characters[i];
                    
                    pojo.Player = level.Player;

                    serializer.Serialize(writer, pojo);
                    writer.Close();
                }

                /// <summary>
                /// Načte level ze souboru
                /// </summary>
                /// <param name="path">Cesta k souboru</param>
                /// <returns>Level</returns>
                public Level Deserialize(string path)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(LevelPojo));
                    StreamReader streamReader = new StreamReader(path);

                    //TODO: Maximum Width and Height
                    LevelPojo pojo = (LevelPojo) serializer.Deserialize(streamReader);
                    Level level = new Level(pojo.Width, pojo.Height);

                    foreach (var pojoBlock in pojo.Blocks)
                    {
                        if(pojoBlock.X % 10 != 0.0)
                            throw new ArgumentException($"Invalid block position. The object position should be moduled by 10 {pojoBlock.X}, {pojoBlock.Y}");
                        if(pojoBlock.X >= Level.MaxWidth || pojoBlock.Y >= Level.MaxHeight)
                            throw new ArgumentException($"Invalid block position. The object is out of playground {pojoBlock.X}, {pojoBlock.Y}");
                        if(!level.Add(pojoBlock))
                            throw new ArgumentException($"Invalid block position. There is already some object at {pojoBlock.X}, {pojoBlock.Y}");
                    }

                    foreach (var pojoEnemy in pojo.Enemies)
                    {
                        level.Blocks.ForEach(o =>
                        {
                            if(BaseGame.CollisionCheck(o, pojoEnemy)) 
                                throw new AggregateException($"Invalide character position. It colides with object [{o.X}, {o.Y}]");
                        });
                        if(pojoEnemy.X >= Level.MaxWidth || pojoEnemy.Y >= Level.MaxHeight)
                            throw new ArgumentException($"Invalid character position. The character is out of playground {pojoEnemy.X}, {pojoEnemy.Y}");
                        BaseGame.CreateEnemy(pojoEnemy, level);
                        pojoEnemy._player = pojo.Player;
                    }
                    if(pojo.Player.X >= Level.MaxWidth || pojo.Player.Y >= Level.MaxHeight)
                        throw new ArgumentException($"Invalid player position. The character is out of playground {pojo.Player.X}, {pojo.Player.Y}");
                    level.Player = pojo.Player;
                    level.LoadedHash = pojo.Hash;

                    return level;
                }
            }
        }
    }
}