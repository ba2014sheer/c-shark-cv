﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Serialization;
using Feichero.Game;
using Feichero.Game.Character;
using Feichero.Gui;

namespace Feichero
{
    /// <summary>
    /// Herní level hry s logikou nepřátel
    /// </summary>
    public class Level
    {
        /// <summary>
        /// Maximalni sirka bloku
        /// </summary>
        public static readonly  int MaxHeight = 12 * Components.BlockSize;
        /// <summary>
        /// Maximalni sirka bloku
        /// </summary>
        public static readonly  int MaxWidth = 16 * Components.BlockSize;
        /// <summary>
        /// Hash po načtení ze souboru
        /// </summary>
        public int LoadedHash;

        /// <summary>
        /// Definuje šířku herní plochy v blocích (nikoliv pixelech)
        /// </summary>
        private int _width;

        /// <summary>
        /// Vrátí šířku herní mapy v blocích
        /// </summary>
        public int Width
        {
            get => _width;
            set => _width = value;
        }

        /// <summary>
        /// Definuje výšku herní plochy v blocích (nikoliv pixelech)
        /// </summary>
        private int _height;

        /// <summary>
        /// Vrátí výšku herní mapy v blocích
        /// </summary>
        public int Height
        {
            get => _height;
            set => _height = value;
        }

        /// <summary>
        /// Definuje postavy 
        /// </summary>
        public List<ICharacter> Characters { get; }

        /// <summary>
        /// Pohybující se střely po herní mapě
        /// </summary>
        [XmlIgnore]
        public List<IObject> Bullets { get; }

        /// <summary>
        /// Nepohyblivé předměty
        /// </summary>
        public List<IObject> Blocks { get; }

        /// <summary>
        /// Herní mřížka
        /// </summary>
        private IObject[,] _playground;

        /// <summary>
        /// Hráč
        /// </summary>
        private Player _player;

        /// <summary>
        /// Event handler po přidání nového objektu
        /// </summary>
        /// <param name="???"></param>
        public delegate void NewObjectAddedEventHandler(IObject o);

        /// <summary>
        /// Událost po přidání objektu
        /// </summary>
        public event NewObjectAddedEventHandler ObjectCreaded;

        /// <summary>
        /// Hledač cesty
        /// </summary>
        private PathFinder PathFinder;

        /// <summary>
        /// Semafor pro řízení přístupu k postavám
        /// </summary>
        public Semaphore CharacterSemaphore { get; }
        /// <summary>
        /// Semafor pro řízení přístupu ke strelam
        /// </summary>
        public Semaphore BulletSemaphore { get; }

        /// <summary>
        /// Getter a Setter pro hráče
        /// </summary>
        public Player Player
        {
            get => _player;
            set
            {
                _player = value;
                _player.Level = this;
            }
        }

        /// <summary>
        /// Konstruktor pro XML serializaci
        /// </summary>
        public Level()
        {
            PathFinder = new PathFinder(Width, Height);
            CharacterSemaphore = new Semaphore(1, 1);
            BulletSemaphore = new Semaphore(1, 1);
        }

        /// <summary>
        /// Vytvoří herní level
        /// </summary>
        /// <param name="width">Šíška hrací plochy</param>
        /// <param name="height"></param>
        public Level(int width, int height) : this()
        {
            _width = width;
            _height = height;
            Characters = new List<ICharacter>();
            Bullets = new List<IObject>();
            Blocks = new List<IObject>();
            _playground = new IObject[width + 1, height + 1];
            PathFinder = new PathFinder(Width, Height);
            //Oprava pokud je zadána příliš malá plocha
            if (width < 2)
                Width = 2;
            if (height < 2)
                Height = 2;
            //Vytvoř ohraničení
            for (var i = 0; i < Width + 1; i++)
            {
                Blocks.Add(new Block(i * Components.BlockSize, 0));
                Blocks.Add(new Block(i * Components.BlockSize, Height * Components.BlockSize));
            }

            for (var i = 1; i < Height; i++)
            {
                Blocks.Add(new Block(0, i * Components.BlockSize));
                Blocks.Add(new Block(Width * Components.BlockSize, i * Components.BlockSize));
            }
        }

        /// <summary>
        ///  Pohne všema postavama
        /// </summary>
        public void MoveAll()
        {
            for (var i = 0; i < Characters.Count; ++i)
            {
                Characters[i].Move();
            }
            
            for (var i = 0; i < Bullets.Count; ++i)
            {
                Bullets[i].Move();
            }
        }

        /// <summary>
        /// Vypíše stav levelu
        /// </summary>
        /// <returns>Textový popis stavu levelu</returns>
        public override string ToString()
        {
            return $"[{LoadedHash}] {{ Počet nepřátel: {Characters.Count} }} ";
        }

        /// <summary>
        /// Přidá objekt do levelu
        /// </summary>
        /// <param name="o">Objekt na přidání</param>
        public bool Add(IObject o)
        {
            int x = (int) (o.X / Components.BlockSize);
            int y = (int) (o.Y / Components.BlockSize);
            
            var free = _playground[x, y] == null;
            _playground[x, y] = o;

            return free;
        }

        /// <summary>
        /// Přidá postavu do levelu
        /// </summary>
        /// <param name="c">Postava na přidání</param>
        public void Add(Character c)
        {
            Characters.Add(c);
        }

        /// <summary>
        /// Vrátí překážku na daných souřadnicích
        /// </summary>
        /// <param name="x">Xová souřadnice</param>
        /// <param name="y">Yová souřadnice</param>
        public IObject this[int x, int y]
        {
            get => _playground[x, y];
            set
            {
                if (value == null || _playground[x, y] != null)
                    Blocks.Remove(_playground[x, y]);
                else
                    Blocks.Add(value);
                _playground[x, y] = value;
            }
        }

        /// <summary>
        /// Porovnání dvou levelů z hlediska Hashe při načítání souborů
        /// </summary>
        /// <param name="obj">Druhý level</param>
        /// <returns>true/false shodné levely</returns>
        public override bool Equals(object obj)
        {
            if (obj != null && obj.GetType() == typeof(Level))
                return LoadedHash == ((Level) obj).LoadedHash;
            return false;
        }

        /// <summary>
        /// Hash kod je určen při načítání levelu
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return LoadedHash;
        }

        /// <summary>
        /// Provede akci všech postav
        /// <param name="millis">Milisekundy z timeru</param>
        /// </summary>
        public void ActionAll(double millis)
        {
            Characters.ForEach(ch => ch.Action(millis));
        }

        /// <summary>
        /// Přidá nový pohyblivý objekt do hry
        /// </summary>
        /// <param name="bullet">Pohyblivý objekt</param>
        /// <exception cref="NotImplementedException"></exception>
        public void AddBullet(Bullet bullet)
        {
            bullet.ObjectDelete += delegate
            {
                BulletSemaphore.WaitOne();
                Bullets.Remove(bullet);
                BulletSemaphore.Release();
            };
            Bullets.Add(bullet);
            ObjectCreaded?.Invoke(bullet);
        }

        /// <summary>
        /// Najde cestu v levelu
        /// </summary>
        /// <param name="start">počáteční objekt</param>
        /// <param name="target">cílový objekt</param>
        /// <returns>délka cesty</returns>
        public PathFinder.PricePath[,] FindMeAPath(IObject start, IObject target)
        {
            return PathFinder.FindMeAPath(_playground, start, target);
        }
    }
}