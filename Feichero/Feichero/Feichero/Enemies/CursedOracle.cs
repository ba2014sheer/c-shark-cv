﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using Feichero.Gui;

namespace Feichero
{
    namespace Game
    {
        namespace Character
        {
            /// <summary>
            /// Jednoduchý nepřítel, přibližuje se k hráčovi stále zvyšující se rychlostí.
            /// </summary>
            public class CursedOracle : Enemy
            {
                /// <summary>
                /// Tolerance ke kolizi
                /// </summary>
                public static double COLLISION_TOLLERACE = 0.2;
                /// <summary>
                /// Cas pristiho utoku
                /// </summary>
                [XmlIgnore]
                public double NextAttack { get; set; }
                /// <summary>
                /// Cas od posledniho utoku
                /// </summary>
                [XmlIgnore]
                public int LastAttackFrom { get; set; }
                /// <summary>
                /// Posledni znama cesta k hracovi
                /// </summary>
                [XmlIgnore]
                private PathFinder.PricePath[,] _pathToPlayer;
                /// <summary>
                /// Posledni znama lokace hrace
                /// </summary>
                [XmlIgnore]
                private Point _playerLocation;
                public CursedOracle(EnemyKind kind, int x, int y, int life, Attack attackKind) : base(kind, x, y, life, attackKind)
                {
                    NextAttack = AttackSpeed;
                }

                public CursedOracle(EnemyKind kind, int x, int y, int width, int height, int life, Attack attackKind) : base(kind, x, y, width, height, life, attackKind)
                {
                    NextAttack = AttackSpeed;
                }
                /// <summary>
                /// Kontruktor pro serializaci
                /// </summary>
                public CursedOracle() : base(EnemyKind.SERIALIZED, 0, 0, 0, Attack.SERIALIZED)
                {
                    NextAttack = AttackSpeed;
                }

                public override void Move()
                {
                    if (_pathToPlayer == null || _playerLocation.X != _player.X || _playerLocation.Y != _player.Y) { //Cashování nehledej cestu pokud hráč nezměnil svojí polohu
                        _pathToPlayer = Level.FindMeAPath(this, _player);
                        _playerLocation.X = (int) (_player.X / Components.BlockSize);
                        _playerLocation.Y = (int) (_player.Y / Components.BlockSize);
                    }

                    if (_pathToPlayer != null)
                    {
                        Debug.WriteLine(PrintPath());
                        
                        int curX = (int) (X / Components.BlockSize);
                        int curY = (int) (Y / Components.BlockSize);
                        int curDist = _pathToPlayer[curX, curY].Dist;
                        
                        List<Tuple<Direction, int>> possiblePaths = new List<Tuple<Direction, int>>();
                        if (_pathToPlayer[curX - 1, curY] != null){
                            possiblePaths.Add(new Tuple<Direction, int>(Direction.LEFT, _pathToPlayer[curX - 1, curY].Dist));
                        }
                        if (_pathToPlayer[curX + 1, curY] != null){
                            possiblePaths.Add(new Tuple<Direction, int>(Direction.RIGHT, _pathToPlayer[curX + 1, curY].Dist));
                        }
                        if (_pathToPlayer[curX, curY - 1] != null){
                            possiblePaths.Add(new Tuple<Direction, int>(Direction.UP, _pathToPlayer[curX, curY - 1].Dist));
                        }
                        if (_pathToPlayer[curX, curY + 1] != null){
                            possiblePaths.Add(new Tuple<Direction, int>(Direction.DOWN, _pathToPlayer[curX, curY + 1].Dist));
                        }
                        
                        
                        //Pokud uz maji kolizi
                        //Debug.WriteLine(BaseGame.CollisionOverlap(this, _player) < COLLISION_TOLLERACE);
                        if (BaseGame.CollisionCheck(this, _player) && BaseGame.CollisionOverlap(this, _player) < COLLISION_TOLLERACE)
                        {
                            if (X > _player.X && !CheckCollision(X - Speed, Y)) 
                                X = X - Speed;
                            if (X < _player.X && !CheckCollision(X + Speed, Y)) 
                                X = X + Speed;
                            if (Y > _player.Y && !CheckCollision(X, Y - Speed)) 
                                Y = Y - Speed;
                            if (Y < _player.Y && !CheckCollision(X, Y + Speed)) 
                                Y = Y + Speed;
                            OnObjectMoved((int) X, (int) Y);
                            return;
                        }
                        
                        List<Direction> directions = FindTheBestPath(possiblePaths);
                            
                        directions.ForEach(direction => {
                            switch (direction) {
                                case Direction.LEFT:
                                    X = X-Speed;
                                    break;
                                case Direction.RIGHT:
                                    X = X + Speed;
                                    break;
                                case Direction.DOWN:
                                    Y = Y+Speed;
                                    break;
                                case Direction.UP:
                                    Y = Y-Speed;
                                    break;
                            }
                            //Debug.WriteLine(direction);
                        });
                        OnObjectMoved((int) X, (int) Y);
                    }
                }

                private List<Direction> FindTheBestPath(List<Tuple<Direction, int>> possiblePaths)
                {
                    List<Direction> directions = new List<Direction>();
                    possiblePaths.Sort((t1, t2) => t1.Item2 - t2.Item2);
                    possiblePaths.ForEach(tuple => {
                        if(tuple.Item2 != Int32.MaxValue)
                            switch (tuple.Item1) {
                                case Direction.LEFT:
                                    if(!CheckCollision(X-Speed, Y) && !directions.Contains(Direction.RIGHT)) 
                                        directions.Add(tuple.Item1);
                                    break;
                                case Direction.RIGHT:
                                    if(!CheckCollision(X+Speed, Y) && !directions.Contains(Direction.LEFT)) 
                                        directions.Add(tuple.Item1);
                                    break;
                                case Direction.UP:
                                    if(!CheckCollision(X, Y-Speed) && !directions.Contains(Direction.DOWN)) 
                                        directions.Add(tuple.Item1);
                                    break;
                                case Direction.DOWN:
                                    if(!CheckCollision(X, Y+Speed) && !directions.Contains(Direction.UP)) 
                                        directions.Add(tuple.Item1);
                                    break;
                        }
                    });
                    return directions;
                }
                
                /// <summary>
                /// Zkontroluje kolizi
                /// </summary>
                /// <param name="x">Xova</param>
                /// <param name="y">Yonova</param>
                /// <returns>true koliduje, false - nekoliduje</returns>
                private bool CheckCollision(double x, double y) {
                    bool collide = false;
                    Blocks.ForEach(o =>
                    {
                        if (BaseGame.CollisionCheck((int) x, (int) y, Width, Height, o)) 
                            collide = true;
                    });
                    return collide;
                }

                public override void Action(double intervalTotalMilliseconds)
                {
                    if (LastAttackFrom++ >= NextAttack && BaseGame.CollisionCheck(this, _player) && BaseGame.CollisionOverlap(this, _player) > COLLISION_TOLLERACE)
                    {
                        _player.AttackTo(this);
                        //Debug.WriteLine($"{ToString()} - Attacking to {_player}" );
                        NextAttack = AttackSpeed;
                        LastAttackFrom = 0;
                    }
                }
                
                /// <summary>
                /// Vykresli cestu k hraci
                /// </summary>
                /// <returns>cesta</returns>
                private string PrintPath()
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < _pathToPlayer.GetLength(1); i++)
                    {
                        for (int j = 0; j < _pathToPlayer.GetLength(0); j++)
                        {
                            if(_pathToPlayer[j, i] != null) 
                                builder.Append($"{_pathToPlayer[j, i].Dist}");
                            else
                                builder.Append("X");
                        }
                        builder.Append("\n");
                    }
                    return builder.ToString();
                }
            }
        }
    }
}