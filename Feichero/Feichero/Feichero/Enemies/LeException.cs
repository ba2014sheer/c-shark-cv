﻿using System.Xml.Serialization;

namespace Feichero
{
    namespace Game
    {
        namespace Character
        {
            /// <summary>
            /// Nepřítel který stojí a po hráčovi střílí vzdušné střeli v určitých intervalech.
            /// </summary>
            public class LeException : Enemy
            {
                /// <summary>
                /// Cas pristi strely
                /// </summary>
                [XmlIgnore]
                protected int NextShootTime { get; set; }
                /// <summary>
                /// Cas od poslední strely
                /// </summary>
                [XmlIgnore]
                protected int NextShoot { set; get; }
                /// <summary>
                /// Cas pristiho vystreleni (min) 
                /// </summary>
                [XmlIgnore]
                public static readonly int NextShootTimeMin = 220;
                /// <summary>
                /// Cas pristiho vystreleni (max) 
                /// </summary>
                [XmlIgnore]
                public static readonly int NextShootTimeMax = 390;
                /// <summary>
                /// Kontruktor pro serializaci
                /// </summary>
                public LeException() : base(EnemyKind.SERIALIZED, 0, 0, 0, Attack.SERIALIZED)
                {
                    NextShoot = 0;
                    NextShootTime = Random.Next(NextShootTimeMin, NextShootTimeMax);
                }

                
                public LeException(EnemyKind kind, int x, int y, int life, Attack attackKind) : base(kind, x, y, life, attackKind)
                {
                    NextShoot = 0;
                    NextShootTime = Random.Next(NextShootTimeMin, NextShootTimeMax);
                }

                public LeException(EnemyKind kind, int x, int y, int width, int height, int life, Attack attackKind) : base(kind, x, y, width, height, life, attackKind)
                {
                    NextShoot = 0;
                    NextShootTime = Random.Next(NextShootTimeMin, NextShootTimeMax);
                }

                public override void Move()
                {
                    //Postava se nehýbe
                }

                public override void Action(double intervalTotalMilliseconds)
                {
                    if (NextShoot++ >= AttackSpeed)
                    {
                        var x = _player.X + _player.Width / 2;
                        var y = _player.Y + _player.Height / 2;

                        var bullet = new Bullet((int) X, (int) Y, (int) x, (int) y);
                        bullet.DestinationReached += delegate(int dx, int dy) {
                            if(BaseGame.CollisionCheck(bullet, _player))
                                _player.AttackTo(this);
                        };
                        bullet.ObjectMoved += (nX, nY, pX, pY) =>
                        {
                            if (BaseGame.CollisionCheck(bullet, _player))
                            {
                                _player.AttackTo(this);
                                bullet.Delete();
                            }
                        };
                        Level.AddBullet(bullet);
                        
                        NextShoot = 0;
                        NextShootTime = Random.Next(NextShootTimeMin, NextShootTimeMax);
                    }
                }
            }
        }
    }
}