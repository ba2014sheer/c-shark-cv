﻿using System.Xml.Serialization;

namespace Feichero {
    namespace Game {
        namespace Character {
            public class SegmentTheFaulted : CursedOracle
            {
                /// <summary>
                /// Vyssi rychlost
                /// </summary>
                public static readonly double SpeedFast = 2.25;
                /// <summary>
                /// Nizsi rychlost
                /// </summary>
                public static readonly double SpeedSlow = 3.5;
                /// <summary>
                /// Horni hranice pro zmenu rychlosti
                /// </summary>
                [XmlIgnore]
                public static readonly int SpeedChangeUpTime = 700;
                /// <summary>
                /// Dolni hranice pro zmenu rychlosti
                /// </summary>
                [XmlIgnore]
                public static readonly int SpeedChangeDownTime = 180;
                /// <summary>
                /// Detekuje cas od posledni změny rychlost
                /// </summary>
                [XmlIgnore]
                public int LastSpeedChange;
                /// <summary>
                /// Pristi zmena rychlosti
                /// </summary>
                [XmlIgnore]
                public int NextSpeed;
                /// <summary>
                /// Detekuje jestli postava je rychlejsi
                /// </summary>
                [XmlIgnore]
                public bool OnSpeed;
                /// <summary>
                /// Kontruktor pro serializaci
                /// </summary>
                public SegmentTheFaulted() : base(EnemyKind.SERIALIZED, 0, 0, 0, Attack.SERIALIZED)
                {
                    Damage = 25;
                    NextSpeed = Random.Next(SpeedChangeDownTime, SpeedChangeUpTime);
                }

                
                public SegmentTheFaulted(EnemyKind kind, int x, int y, int life, Attack attackKind) : base(kind, x, y, life, attackKind)
                {
                    Damage = 25;
                    NextSpeed = Random.Next(SpeedChangeDownTime, SpeedChangeUpTime);
                }

                public SegmentTheFaulted(EnemyKind kind, int x, int y, int width, int height, int life, Attack attackKind) : base(kind, x, y, width, height, life, attackKind)
                {
                    Damage = 25;
                    NextSpeed = Random.Next(SpeedChangeDownTime, SpeedChangeUpTime);
                }

                public override void Action(double intervalTotalMilliseconds)
                {
                    if (LastSpeedChange++ >= NextSpeed)
                    {
                        Speed = (OnSpeed) ? SpeedSlow : SpeedFast;

                        OnSpeed = !OnSpeed;
                        NextSpeed = Random.Next(SpeedChangeDownTime, SpeedChangeUpTime);
                        LastSpeedChange = 0;
                    }
                    base.Action(intervalTotalMilliseconds);
                }
            }   
        }
    }
}