﻿using System;
using System.Xml.Serialization;

namespace Feichero
{
    namespace Game
    {
        namespace Character
        {
            /// <summary>
            /// Druh nepřítele
            /// </summary>
            public enum EnemyKind
            {
                /// <summary>
                /// Jednoduchý nepřítel, přibližuje se k hráčovi stále zvyšující se rychlostí.
                /// </summary>
                CURSED_ORACLE,
                /// <summary>
                /// Jednoduchý nepřítel s teleportací, náhodně se prochází po herní mapě a v určitou chvíli se dokáže
                /// teleportovat k hráčovi. Teleportace má prodlevu a hráč jí vidí.
                /// </summary>
                MELTED_CARLO,
                /// <summary>
                /// Jednoduší nepřítel, přibližuje se k hráčovi. V náhodných intervalech rapidně zvyšuje rychlost.
                /// </summary>
                SEGMENT_THE_FAULTED,
                /// <summary>
                /// Středně těžký nepřítel. Nemůže se hýbat a v určitých interval stříli po hráči vzdušné střely
                /// </summary>
                LE_EXCEPTION,
                /// <summary>
                /// Bude urceno po serializaci
                /// </summary>
                SERIALIZED
            }
            
            /// <summary>
            /// Postava představující nepřítele v herním světe
            /// </summary>
            [XmlInclude(typeof(CursedOracle)), XmlInclude(typeof(MeltedCarlo)), 
             XmlInclude(typeof(LeException)), XmlInclude(typeof(SegmentTheFaulted))]
            [Serializable]
            public abstract class Enemy : Character
            {
                /// <summary>
                /// Počet nepřátel ve hře
                /// </summary>
                public static readonly int Count = 5;
                /// <summary>
                /// Hráč
                /// </summary>
                [XmlIgnore]
                public Player _player { get; set; }
                /// <summary>
                /// Druh nepřítele
                /// </summary>
                public EnemyKind Kind { get; set; }

                /// <summary>
                /// Vytvoří herní postavu.
                /// </summary>
                /// <param name="x">Xová souřadnice</param>
                /// <param name="y">Yonová souřadnice</param>
                /// <param name="life">Život postavy</param>
                /// <param name="attackKind">Druh útoku</param>
                public Enemy(EnemyKind kind, int x, int y, int life, Attack attackKind) : base(x, y, life, attackKind)
                {
                    Kind = kind;
                    base.LifeChange += (current, previous, max) =>
                    {
                        if (current <= 0) base.OnDelete();
                    };
                }

                /// <summary>
                /// Vytvoří herní postavu.
                /// </summary>
                /// <param name="x">Xová souřadnice</param>
                /// <param name="y">Yonová souřadnice</param>
                /// <param name="width">Šírka postavy</param>
                /// <param name="height">Víška postavy</param>
                /// <param name="life">Život postavy</param>
                /// <param name="attackKind">Druh útoku</param>
                public Enemy(EnemyKind kind, int x, int y, int width, int height, int life, Attack attackKind) : base(x,
                    y, width, height, life, attackKind)
                {
                    Kind = kind;
                    base.LifeChange += (current, previous, max) =>
                    {
                        if (current <= 0) base.OnDelete();
                    };
                }
                /// <summary>
                /// Vzdálenost k hráčovi
                /// </summary>
                /// <returns>vzdálenost</returns>
                protected double DistanceToPlayer() {
                    var vectorX = _player.X - X;
                    var vectorY = _player.Y - Y;
                    var distance = Math.Sqrt(Math.Pow(vectorX, 2) + Math.Pow(vectorY, 2));

                    return distance;
                }
                /// <summary>
                /// Vypočítá vektory k hráčovi
                /// </summary>
                /// <param name="v1">Vektor 1 X</param>
                /// <param name="v2">Vektor 2 X</param>
                /// <returns>Vrací true pokud se postava přiblížila k hráčovi</returns>
                protected bool PathToPlayer(out double v1, out double v2)
                {
                    v1 = _player.X - X;
                    v2 = _player.Y - Y;

                    //TODO
                    return false;
                }
                
                public override void AttackTo(Character attacker)
                {
                    int p = Life;
                    Life = Life - attacker.Damage;
                    OnLifeChange(p);
                }
                
                public override string ToString() {
                    return $"{Kind.ToString()} [{X},{Y}] ({Life}/{MaxLife}) ";
                }
            }
        }
    }
}