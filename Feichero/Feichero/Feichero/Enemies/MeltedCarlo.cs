﻿using System.Xml.Serialization;

namespace Feichero
{
    namespace Game
    {
        namespace Character
        {
            /// <summary>
            /// Melted Carlo je nepřítel který se náhodně prochází po herní mapě a náhodný čas se teleportuje blízko
            /// hráče.
            /// </summary>
            public class MeltedCarlo : CursedOracle
            {
                /// <summary>
                /// Čas teleportace
                /// </summary>
                [XmlIgnore]
                public static readonly int TeleportingTime = 150;

                /// <summary>
                /// Minimální čas pro teleportaci
                /// </summary>
                [XmlIgnore]
                public static readonly int TeleportationTimeMin = 500;

                /// <summary>
                /// Maximální čas pro teleportaci 
                /// </summary>
                [XmlIgnore]
                public static readonly int TeleportationTimeMax = 650;

                /// <summary>
                /// Minimální vzdálenost pro teleportaci k nepříteli
                /// </summary>
                [XmlIgnore]
                public static readonly int TeleportationMin = 5;

                /// <summary>
                /// Maximální vzdálenost pro teleportaci k nepříteli
                /// </summary>
                [XmlIgnore]
                public static readonly int TeleportationMax = 20;

                /// <summary>
                /// Minimální rozestup Melted Carla od hráče aby jej začal pronásledovat
                /// </summary>
                [XmlIgnore]
                public static readonly int FollowEnemy = 50;

                /// <summary>
                /// Určuje čas kdy se postava teleportuje
                /// </summary>
                [XmlIgnore]
                protected long NextTeleportation { set; get; }

                /// <summary>
                /// Čas od poslední teleportace
                /// </summary>
                [XmlIgnore]
                protected long TimeFromLastTeleportation { get; set; }

                /// <summary>
                /// Určuje čas do teleportace
                /// </summary>
                [XmlIgnore]
                protected long TimeToTeleportation { get; set; }

                /// <summary>
                /// Detekuje jestli se nepřítel zrovnat teleportuje
                /// </summary>
                [XmlIgnore]
                protected bool isTeleporting = false;

                /// <summary>
                /// Teleportovací Xová souřadnice
                /// </summary>
                [XmlIgnore]
                protected int TelX;

                /// <summary>
                /// Teleportovací Yonová souřadnice
                /// </summary>
                [XmlIgnore]
                protected int TelY;

                /// <summary>
                /// Kontruktor pro serializaci
                /// </summary>
                public MeltedCarlo() : base(EnemyKind.MELTED_CARLO, 0, 0, 0, Attack.MELEE)
                {
                    TimeFromLastTeleportation = Random.Next(TeleportationTimeMin, TeleportationTimeMax);
                    NextTeleportation = 0;
                }

                /// <summary>
                /// 
                /// </summary>
                /// <param name="kind"></param>
                /// <param name="x"></param>
                /// <param name="y"></param>
                /// <param name="life"></param>
                /// <param name="attackKind"></param>
                public MeltedCarlo(EnemyKind kind, int x, int y, int life, Attack attackKind) : base(kind, x, y, life,
                    attackKind)
                {
                    TimeFromLastTeleportation = Random.Next(TeleportationTimeMin, TeleportationTimeMax);
                    NextTeleportation = 0;
                }

                /// <summary>
                /// 
                /// </summary>
                /// <param name="kind"></param>
                /// <param name="x"></param>
                /// <param name="y"></param>
                /// <param name="width"></param>
                /// <param name="height"></param>
                /// <param name="life"></param>
                /// <param name="attackKind"></param>
                public MeltedCarlo(EnemyKind kind, int x, int y, int width, int height, int life, Attack attackKind) :
                    base(kind, x, y, width, height, life, attackKind)
                {
                    TimeFromLastTeleportation = Random.Next(TeleportationTimeMin, TeleportationTimeMax);
                    NextTeleportation = 0;
                }

                /// <summary>
                /// Posune postavu. Melted Carlo se hýbe náhodným směrem, jenom když je v blízkosti nepřítele tak se přibližuje
                /// </summary>
                public override void Move()
                {
                    if(!isTeleporting)
                        base.Move();
                }

                public override void Action(double intervalTotalMilliseconds)
                {
                    if (isTeleporting && NextTeleportation++ >= TimeToTeleportation) { //Mam se teleportovat?
                        isTeleporting = false;
                        NextTeleportation = 0;
                        TimeFromLastTeleportation = Random.Next(TeleportationTimeMin, TeleportationTimeMax);
                        var pX = X;
                        var pY = Y;
                        X = TelX;
                        Y = TelY;
                        OnObjectMoved((int) pX, (int) pY, (int) X, (int) Y);
                    }
                    
                    if(!isTeleporting)
                        base.Action(intervalTotalMilliseconds);

                    if (NextTeleportation++ < TimeFromLastTeleportation || BaseGame.Distance(this, _player) <= TeleportationMax)
                            return;

                    int x = (int) (_player.X + Random.Next(TeleportationMin, TeleportationMax));
                    int y = (int) (_player.Y + Random.Next(TeleportationMin, TeleportationMax));

                    var canITeleport = true;
                    Blocks.ForEach(b =>
                    {
                        if (BaseGame.CollisionCheck(x, y, Width, Height, b)) canITeleport = false;
                    });

                    if (canITeleport)
                    {
                        
                        TelX = x;
                        TelY = y;

                        isTeleporting = true;
                        TimeToTeleportation = TeleportingTime;
                        NextTeleportation = 0;
                    }
                }
            }
        }
    }
}