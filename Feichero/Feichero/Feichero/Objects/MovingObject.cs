﻿namespace Feichero
{
    namespace Game
    {
        public abstract class MovingObject : IObject
        {
            /// <summary>
            /// Handler pro událost kdy je s postavou pohnuto
            /// </summary>
            /// <param name="x">Současná Xová souřadnice</param>
            /// <param name="y">Současná Yová souřadnice</param>
            /// <param name="pX">Předchozí Xová souřadnice</param>
            /// <param name="pY">Předchozí Yová souřadnice</param>
            public delegate void ObjectMovedEventHandler(int x, int y, int pX, int pY);
            /// <summary>
            /// Událost aktivovaná s pohybem postavy
            /// </summary>
            public event ObjectMovedEventHandler ObjectMoved;
            /// <summary>
            /// Handler pro smazání objektu
            /// </summary>
            /// <param name="objects">Ostatní objekty v herním světě</param>
            public delegate void ObjectDeleteEventHandler();
            /// <summary>
            /// Událost aktivovaná smazáním postavy
            /// </summary>
            public event ObjectDeleteEventHandler ObjectDelete;
            /// <summary>
            /// Aktiovuje události
            /// </summary>
            /// <param name="previous">Předchozí počet životů</param>
            protected void OnDelete()
            {
                var handler = ObjectDelete;
                handler?.Invoke();
            }
            /// <summary>
            /// Aktivuje události
            /// </summary>
            /// <param name="Px">Předchozí Xová souřadnice</param>
            /// <param name="Py">Předchozí Yová souřadnice</param>
            protected void OnObjectMoved(int Px, int Py)
            {
                var handler = ObjectMoved;
                handler?.Invoke((int) X, (int) Y, Px, Py);
            }
            public double X { get; set; }
            public double Y { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public abstract void Move();
            public abstract void Action(double intervalTotalMilliseconds);
            public void Delete() {
                ObjectDelete?.Invoke();
            }

            protected virtual void OnObjectMoved(int x, int y, int px, int py)
            {
                ObjectMoved?.Invoke(x, y, px, py);
            }
        }
    }
}