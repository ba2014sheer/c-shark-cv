﻿using System;
using Feichero.Gui;

namespace Feichero{
    namespace Game
    {
        /// <summary>
        /// Základní objekt v herním světě
        /// </summary>
        public interface IObject
        {
            /// <summary>
            /// Definuje Xovou souřadnici kde se objekt nachází
            /// </summary>
            double X { get;}

            /// <summary>
            /// Definuje Yovou souřadnici kde se objekt nachází
            /// </summary>
            double Y { get;}

            /// <summary>
            /// Udává šířku vykreslovaného objektu
            /// </summary>
            int Width { get;}

            /// <summary>
            /// Udává výšku vykreslovaného objektu
            /// </summary>
            int Height { get;}

            /// <summary>
            /// Pohne se (změní souřadnice) v herním světě
            /// </summary>
            void Move();
            
            /// <summary>
            /// Provede akci
            /// </summary>
            void Action(double intervalTotalMilliseconds);
        }
        /// <summary>
        /// Základní blok herního světa.
        /// </summary>
        [Serializable]
        public class Block : IObject {
            /// <summary>
            /// Definuje Xovou souřadnici kde se objekt nachází
            /// </summary>
            public double X { get; set; }

            /// <summary>
            /// Definuje Yovou souřadnici kde se objekt nachází
            /// </summary>
            public double Y { get; set; }

            /// <summary>
            /// Udává šířku vykreslovaného objektu
            /// </summary>
            public int Width { get; set; }

            /// <summary>
            /// Udává výšku vykreslovaného objektu
            /// </summary>
            public int Height { get; set; }

            /// <summary>
            /// Postava se pohne (změní souřadnice) v herním světě
            /// </summary>
            public void Move(){
            }

            /// <summary>
            /// Postava provede akci
            /// </summary>
            public void Action(double intervalTotalMilliseconds) {
            }

            public void Delete() {
            }

            /// <summary>
            /// Vytvoří blok
            /// </summary>
            /// <param name="x">Definuje Xovou souřadnici kde se objekt nachází</param>
            /// <param name="y">Definuje Yovou souřadnici kde se objekt nachází</param>
            public Block(int x, int y)
            {
                X = x;
                Y = y;
                Width = Components.BlockSize;
                Height = Components.BlockSize;
            }
            
            /// <summary>
            /// Konstruktor pro serializaci bloku
            /// </summary>
            public Block()
            {
            }
        }
    }
}
