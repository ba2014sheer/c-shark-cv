﻿﻿using System;
using Feichero.Game.Character;

namespace Feichero
{
   namespace Game
   {
       /// <summary>
    /// Střela od začátečního bodu do cílového bodu.
    /// </summary>
    public class Bullet : MovingObject
    {
        /// <summary>
        /// Výchozí šířka střely
        /// </summary>
        public static readonly int BulletWidth = 20;
        /// <summary>
        /// Výchozí výška střely
        /// </summary>
        public static readonly int BulletHeight = 20;
        /// <summary>
        /// Smer X
        /// </summary>
        private int _dx;
        /// <summary>
        /// Smer Y
        /// </summary>
        private int _dy;
        /// <summary>
        /// Krok X
        /// </summary>
        private double _sx;
        /// <summary>
        /// Krok Y
        /// </summary>
        private double _sy;
        /// <summary>
        /// Err
        /// </summary>
        private int _err;
        /// <summary>
        /// Koncová Xová souřadnice střely
        /// </summary>
        public int XEnd { get;}
        /// <summary>
        /// Koncová Yová souřadnice střely
        /// </summary>
        public int YEnd { get;}
        /// <summary>
        /// Rychlost střeli
        /// </summary>
        public double Speed { get;}
        /// <summary>
        /// Event handler na obslužnou událost která nastane po dosažení cíle
        /// </summary>
        /// <param name="x">Cílová xová souřadnice</param>
        /// <param name="y">Cilová ynová souřadnice</param>
        public delegate void DestinationReachedEventHandler(int x, int y);
        /// <summary>
        /// Události dosažení cíle
        /// </summary>
        public event DestinationReachedEventHandler DestinationReached;
        /// <summary>
        /// Smer letu
        /// </summary>
        private Direction Direction { get; }
        /// <summary>
        /// Dosahla cile
        /// </summary>
        public bool Reached { get; set; }

        /// <summary>
        /// Vytvoří novou střelu
        /// </summary>
        /// <param name="x">Xová počáteční</param>
        /// <param name="y">Yonová počáteční</param>
        /// <param name="xEnd">Xová konečná</param>
        /// <param name="yEnd">Yová konečná</param>
        /// <param name="speed">Rychlost strely</param>
        public Bullet(int x, int y, int xEnd, int yEnd, double speed = 1.0)
        {
            X = x;
            Y = y;
            XEnd = xEnd;
            YEnd = yEnd;
            Speed = speed;
            Width = BulletWidth;
            Height = BulletHeight;

            _dx = Math.Abs(x - xEnd);
            _dy = Math.Abs(y - yEnd);

            _sx = x < xEnd ? Speed : -Speed;
            _sy = y < yEnd ? Speed : -Speed;
            
            _err = (_dx > _dy ? _dx : -_dy) / 2;
      
            // initial value of decision 
            // parameter d 

            if (X <= XEnd && Y <= YEnd)
                Direction = Direction.UP_RIGHT;
            else if (X <= XEnd && Y >= YEnd)
                Direction = Direction.DOWN_RIGHT;
            else if (X >= XEnd && Y <= YEnd)
                Direction = Direction.UP_LEFT;
            else
                Direction = Direction.DOWN_LEFT;
        }

        public override void Move()
        {
            int pX = (int) X;
            int pY = (int) Y;
            var e2 = _err;
            
            if (e2 > -_dx) {
                _err -= _dy;
                X += _sx;
            }
            
            if (e2 < _dy) {
                _err += _dx;
                Y += _sy;
            }

            OnObjectMoved((int) X, (int) Y, pX, pY);
            
            if (Reached) {
                Reached = true;
                DestinationReached?.Invoke((int) X, (int) Y);
                Delete();
            }
            switch (Direction)
            {
                case Direction.UP_RIGHT:
                    Reached = (X >= XEnd && Y >= YEnd);
                    break;
                case Direction.DOWN_RIGHT:
                    Reached = (X >= XEnd && Y <= YEnd);
                    break;
                case Direction.UP_LEFT:
                    Reached = (X <= XEnd && Y >= YEnd);
                    break;
                default:
                    Reached = (X <= XEnd && Y <= YEnd);
                    break;
            }
        }

        public override void Action(double intervalTotalMilliseconds) {
        }
    }
   }
}