﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Feichero
{
    /// <summary>
    /// Interaction logic for GameMenu.xaml
    /// </summary>
    public partial class GameMenu : Window
    {
		/// <summary>
                /// Obsluha konce hry
                /// </summary>
                /// <param name="sender"></param>
                public delegate void EndGameEventHandler(object sender);
                /// <summary>
                /// Obsluha přepnutí na další hru
                /// </summary>
                /// <param name="sender"></param>
                public delegate void NextGameEventHandler(object sender);
                /// <summary>
                /// Obsluha přepnutí na předchozí hru
                /// </summary>
                /// <param name="sender"></param>
                public delegate void PreviousGameEventHandler(object sender);
                /// <summary>
                /// Oblsuha pokračování ve hře
                /// </summary>
                /// <param name="sender"></param>
                public delegate void ContinueGameEventHandler(object sender);
                
                /// <summary>
                /// Konec hry event
                /// </summary>
                public event EndGameEventHandler EndGame;
                /// <summary>
                /// Další hra event
                /// </summary>
                public event NextGameEventHandler NextGame;
                /// <summary>
                /// Předchozí hra event
                /// </summary>
                public event PreviousGameEventHandler PreviousGame;
                /// <summary>
                /// Pokračovat ve hře Event
                /// </summary>
                public event ContinueGameEventHandler ContinueGame;

                /// <summary>
                /// Vyvolá události konce hry
                /// </summary>
                /// <param name="sender">odesílatel</param>
                protected void DoEndGame(object sender, RoutedEventArgs args)
                {
                    EndGame?.Invoke(sender);
                }
                /// <summary>
                /// Vyvolá události další hry
                /// </summary>
                /// <param name="sender">odesílatel</param>
                protected void DoNextGame(object sender, RoutedEventArgs args){
                    NextGame?.Invoke(sender);
                }
                /// <summary>
                /// Vyvolá události ulozeni hry
                /// </summary>
                /// <param name="sender">odesílatel</param>
                protected void DoPreviousGame(object sender, RoutedEventArgs args){
                    PreviousGame?.Invoke(sender);
                }
                /// <summary>
                /// Vyvolá události pokračování hry
                /// </summary>
                /// <param name="sender">odesílatel</param>
                protected void DoContinueGame(object sender, RoutedEventArgs args){
                    ContinueGame?.Invoke(sender);
                }

        public GameMenu()
        {
            InitializeComponent();
        }
    }
}
