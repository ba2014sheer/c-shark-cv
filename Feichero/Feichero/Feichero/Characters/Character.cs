﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Feichero
{
    namespace Game
    {
        namespace Character
        {
            /// <summary>
            /// Typ útoku postavy. Dělí se na:
            ///     - Žádný
            ///     - Na blízko
            ///     - Na dálku
            ///     - Kombinovaný
            /// </summary>
            public enum Attack
            {
                NONE,
                MELEE,
                RANGE,
                BOTH,
                /// <summary>
                /// Bude urceno po serializaci
                /// </summary>
                SERIALIZED
            };
            /// <summary>
            /// Směr využívaný při pohybu postavy nebo dálkovém útoku
            /// </summary>
            public enum Direction
            {
                NONE,
                UP,
                DOWN,
                LEFT,
                RIGHT,
                UP_LEFT,
                UP_RIGHT,
                DOWN_LEFT,
                DOWN_RIGHT,
                /// <summary>
                /// Bude urceno po serializaci
                /// </summary>
                SERIALIZED
            }
            /// <summary>
            /// Abstraktní třída reprezentují postavu v herním světě
            /// </summary>
            public abstract class Character : MovingObject, ICharacter
            {
                /// <summary>
                /// Handler pro akci objektu
                /// </summary>
                /// <param name="objects">Ostatní objekty v herním světě</param>
                public delegate void CharacterActionEventHandler(List<IObject> objects);

                /// <summary>
                /// Událost aktivovaná akcí postavy
                /// </summary>
                public event CharacterActionEventHandler CharacterAction;
                /// <summary>
                /// Handler po změně životů
                /// </summary>
                /// <param name="current">Aktuální počet životů</param>
                /// <param name="previous">Předchozí počet životů</param>
                public delegate void CharacterLifeChangeEventHandler(int current, int previous, int max);
                /// <summary>
                /// Událost aktivovaná změnou životů
                /// </summary>
                public event CharacterLifeChangeEventHandler LifeChange;

                /// <summary>
                /// Generátor náhodných čísel
                /// </summary>
                [XmlIgnore] protected Random Random = new Random();

                /// <summary>
                /// Rychlost postavy v herním světě
                /// </summary>
                public double Speed { get; set; }

                /// <summary>
                /// Poškození postavy
                /// </summary>
                public int Damage { set; get; }

                /// <summary>
                /// Rychlost utoku
                /// </summary>
                public int AttackSpeed { set; get; }

                /// <summary>
                /// Definuje maximální počet životů
                /// </summary>
                public int MaxLife;

                /// <summary>
                /// Gettery a settery pro nastavení životu postavy
                /// </summary>
                public int Life;

                /// <summary>
                /// Gettery a settery pro útok
                /// </summary>
                public Attack AttackKind;
                /// <summary>
                /// Bloky v herním světě
                /// </summary>
                [XmlIgnore]
                public List<IObject> Blocks { get; set; }

                /// <summary>
                /// Herní level
                /// </summary>
                [XmlIgnore]
                public Level Level { get; set; }
                /// <summary>
                /// Detekuje jestli je postava naživu
                /// </summary>
                /// <returns>True - naživu, False - mrtvá</returns>
                public bool IsDeath()
                {
                    return Life < 0;
                }
                /// <summary>
                /// Vypočítá život procentuálně
                /// </summary>
                /// <returns>Života v procentech</returns>
                public int lifePercent()
                {
                    return (100 * Life) / MaxLife;
                }

                /// <summary>
                /// Zaútočí na postavu
                /// </summary>
                /// <param name="attacker">Útočník</param>
                public abstract void AttackTo(Character attacker);

                /// <summary>
                /// Vytvoří herní postavu.
                /// </summary>
                /// <param name="x">Xová souřadnice</param>
                /// <param name="y">Yonová souřadnice</param>
                /// <param name="life">Život postavy</param>
                /// <param name="attackKind">Druh útoku</param>
                protected Character(int x, int y, int life, Attack attackKind)
                {
                    X = x;
                    Y = y;
                    Life = life;
                    MaxLife = life;
                    AttackKind = attackKind;
                }
                /// <summary>
                /// Vytvoří herní postavu.
                /// </summary>
                /// <param name="x">Xová souřadnice</param>
                /// <param name="y">Yonová souřadnice</param>
                /// <param name="width">Šírka postavy</param>
                /// <param name="height">Víška postavy</param>
                /// <param name="life">Život postavy</param>
                /// <param name="attackKind">Druh útoku</param>
                protected Character(int x, int y, int width, int height, int life, Attack attackKind)
                {
                    X = x;
                    Y = y;
                    Width = width;
                    Height = height;
                    Life = life;
                    MaxLife = life;
                    AttackKind = attackKind;
                }

                /// <summary>
                /// Výpis postavy
                /// </summary>
                /// <returns></returns>
                public override string ToString()
                {
                    return $"[{X},{Y}] ({Life}/{MaxLife}) {AttackKind} ";
                }
                
                /// <summary>
                /// Aktiovuje události
                /// </summary>
                /// <param name="objects">Objekty herního světa</param>
                protected void OnCharacterAction(List<IObject> objects)
                {
                    var handler = CharacterAction;
                    handler?.Invoke(objects);

                }
                /// <summary>
                /// Aktiovuje události
                /// </summary>
                /// <param name="previous">Předchozí počet životů</param>
                protected void OnLifeChange(int previous)
                {
                    var handler = LifeChange;
                    handler?.Invoke(Life, previous, MaxLife);
                    if (Life <= 0) {
                        Delete();
                    }
                }
                /// <summary>
                /// Zkontroluje jestli nedošlo ke kolizi s překážkou
                /// </summary>
                /// <param name="blocks">Bloky ke kontrole</param>
                /// <param name="pX">Původní hodnota X</param>
                /// <param name="pY">Původní hodnota Y</param>
                protected void RollbackMove(List<IObject> blocks, int pX, int pY)
                {
                    blocks.ForEach(o => {
                        if (BaseGame.CollisionCheck(o, this))
                        {
                            X = pX;
                            Y = pY;
                        }                        
                    });
                }
            }
        }
    }
}