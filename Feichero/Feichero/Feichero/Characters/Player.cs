﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Xml.Serialization;
using Feichero.Gui;

namespace Feichero
{
    namespace Game
    {
        namespace Character
        {
            /// <summary>
            /// Postava, kterou ovládá samotný hráč hry
            /// </summary>
            [Serializable]
            public class Player : Character
            {
                /// <summary>
                /// Gettery a settery pro jméno hráče
                /// </summary>
                public string Name;
                /// <summary>
                /// Čas dalšího útoku v milisekundách
                /// </summary>
                [XmlIgnore]
                private int _nextAttack;
                /// <summary>
                /// Indikuje pohyb postavy
                /// </summary>
                /// <summary>
                /// Stisknuté klávesy
                /// </summary>
                [XmlIgnore]
                public HashSet<Key> Keys { set; protected get; }

                /// <summary>
                /// <param name="name">Jméno hráče</param>
                /// Definuje postavy 
                /// </summary>
                public Player(string name, int life) : base(0, 0, life, Attack.RANGE)
                {
                    Name = name;
                    Damage = 10;
                    _nextAttack = 0;
                }

                /// <summary>
                /// Nastavení parametry hráče a typ útoku na dálkový
                /// </summary>
                /// <param name="name">Jméno hráče</param>
                /// <param name="x">Xová souřadnice</param>
                /// <param name="y">Yonová souřadnice</param>
                /// <param name="life">zivot</param>
                public Player(string name, int x, int y, int life) : base(x, y, life, Attack.RANGE)
                {
                    Name = name;
                    Damage = 10;
                    _nextAttack = 0;
                }
                /// <summary>
                /// Vytvoří herní postavu.
                /// </summary>
                /// <param name="name">Jméno hráče</param>
                /// <param name="x">Xová souřadnice</param>
                /// <param name="y">Yonová souřadnice</param>
                /// <param name="width">Šírka postavy</param>
                /// <param name="height">Víška postavy</param>
                /// <param name="life">Život postavy</param>
                public Player(string name, int x, int y, int width, int height, int life) : base(x, y, width, height,
                    life, Attack.RANGE)
                {
                    Name = name;
                    Damage = 10;
                    _nextAttack = 0;
                }
                    
                /// <summary>
                /// Konstruktor pro serializaci hráče
                /// </summary>
                public Player() : base(0, 0, 0, 0, 0, Attack.RANGE)
                {
                }

                /// <summary>
                /// Postava se pohne (změní souřadnice) v herním světě
                /// </summary>
                public override void Move()
                {
                    double pX = X;
                    double pY = Y;

                    foreach (var key in Keys) {
                        Direction direction = Controls.GetDirectionByKey(key);
                        switch (direction)
                        {
                            case Direction.LEFT:
                                X -= Speed;
                                break;
                            case Direction.RIGHT:
                                X += Speed;
                                break;
                            case Direction.UP:
                                Y -= Speed;
                                break;
                            case Direction.DOWN:
                                Y += Speed;
                                break;
                        }
                    }
                    RollbackMove(Level.Blocks, (int) pX, (int) pY);
                OnObjectMoved((int) pX, (int) pY);
                }

                /// <summary>
                /// Postava p_nextAttackrovede akci
                /// </summary>
                /// <param name="intervalTotalMilliseconds"></param>
                public override void Action(double intervalTotalMilliseconds)
                {
                    if (_nextAttack++ < AttackSpeed || Moving())
                        return;
                    _nextAttack = 0;
                    
                    var enemy = FindNearestShootable(Level.Characters);
                    if (enemy != null)
                    {
                        var xDiff = Math.Abs(enemy.X - X);
                        var yDiff = Math.Abs(enemy.Y - Y);
                        var xEnd = (int) (enemy.X + enemy.Width /2);
                        var yEnd = (int) (enemy.Y + enemy.Height/2);
                        var xStart = (xDiff > Components.BlockSize/2) ? (int) (X + Width / 2) : xEnd;
                        var yStart = (yDiff > Components.BlockSize/2) ? (int) (Y + Height / 2) : yEnd;
                        Bullet bullet = new Bullet(xStart, yStart , xEnd , yEnd);
                        
                        bullet.DestinationReached += delegate
                        {
                            if (BaseGame.CollisionCheck(bullet, enemy)) 
                                enemy.AttackTo(this);
                        };
                        
                        bullet.ObjectMoved += delegate
                        {
                            if (BaseGame.CollisionCheck(bullet, enemy)) bullet.Reached = true;
                        };
                        Level.AddBullet(bullet);
                    }
                }

                public override void AttackTo(Character attacker)
                {
                    int prev = Life;
                    Life -= attacker.Damage;
                    OnLifeChange(prev);
                }
                /// <summary>
                /// Indikuje pohyb postavy
                /// </summary>
                /// <returns></returns>
                public bool Moving()
                {
                    return MainWindow.IsDown;
                }
                /// <summary>
                /// Výpis hráče
                /// </summary>
                /// <returns>Výpis hráče</returns>
                public override string ToString()
                {
                    return $"{base.ToString()} {Name} [PLAYER]";
                }
                /// <summary>
                /// Najde nejbližší cíl k zásahu
                /// </summary>
                /// <param name="objects">Objekty v herním světě</param>
                private ICharacter FindNearestShootable(List<ICharacter> objects)
                {
                    ICharacter nearest = null;
                    var distanceMin = Double.MaxValue;
                    foreach (var enemy in objects) {
                        var vectorX = enemy.X - X;
                        var vectorY = enemy.Y - Y;
                        var distance = Math.Sqrt(Math.Pow(vectorX, 2) + Math.Pow(vectorY, 2));
                        if (enemy != this && distance < distanceMin)
                        {
                            bool shot = true;
                            foreach (var block in Level.Blocks)
                                if (!BaseGame.IsShotable(this,enemy, block)) {
                                    shot = false;
                                    break;
                                }
                            if (shot) {
                                distanceMin = distance;
                                nearest = enemy;
                            }
                        }
                    }
                    return nearest;
                }
            }
        }
    }
}