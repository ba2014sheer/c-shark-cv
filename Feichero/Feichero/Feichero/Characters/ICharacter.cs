﻿namespace Feichero
{
    namespace Game
    {
        namespace Character
        {
            /// <summary>
            /// Rozhraní definující postavu v herním světě
            /// </summary>
            public interface ICharacter : IObject
            {
                void AttackTo(Character attacker);
            }
        }
    }
}