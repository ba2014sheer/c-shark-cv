﻿﻿﻿using System;
using System.Collections.Generic;
using Feichero.Game.Character;
using System.Windows;
using System.Windows.Controls;
  using System.Windows.Input;
using System.Windows.Threading;
using Feichero.Game.FileWorker;
using Feichero.Gui;
  using Microsoft.Win32;
  using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;

  namespace Feichero
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Timer
        /// </summary>
        private DispatcherTimer _gameTickTimer = new DispatcherTimer();
        /// <summary>
        /// Aktuálně hraný level
        /// </summary>
        private readonly Queue<Level> _levels = new Queue<Level>();
        /// <summary>
        /// Stisknuté klávesy
        /// </summary>
        private readonly HashSet<Key> _keys = new HashSet<Key>();
        /// <summary>
        /// Detekuje jestli je stisknutá klávesa
        /// </summary>
        public static bool IsDown { get; set; }
        /// <summary>
        /// Herní menu
        /// </summary>
        private GameMenu _gameMainMenu;
        /// <summary>
        /// Aktualni level
        /// </summary>
        private Level _currentLevel;

        /// <summary>
        /// Konstruktor hlavni hry
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Konstruktor hlavní hry
        /// </summary>
        /// <param name="levels">Levely pro hru</param>
        public MainWindow(HashSet<Level> levels) : this()
        {
            foreach (var level in levels)
                _levels.Enqueue(level);
            SwitchToNextLevel();
        }

        /// <summary>
        /// Přepne na další level
        /// </summary>
        /// <returns>true - je k dispozici další level, false - levely došli</returns>
        private void SwitchToNextLevel()
        {
            if (_levels.Count > 0)
            {
                _gameTickTimer.Stop();
                _gameTickTimer.IsEnabled = false;

                _currentLevel = _levels.Dequeue();
                CreatePlayground(_currentLevel, GameCanvas);

                _gameTickTimer.IsEnabled = true;
                _gameTickTimer.Start();
                return;
            }

            _gameTickTimer.Stop();
            _gameTickTimer.IsEnabled = false;
            WinGame();
        }

        /// <summary>
        /// Hráč hru vyhrál
        /// </summary>
        private void WinGame()
        {
            MessageBox.Show("Tuhle hru jste vyhrál", "Hra končí", MessageBoxButton.OK, MessageBoxImage.Information);
            Close();
        }

        /// <summary>
        /// Hráč hru prohrál hra bude ukončena
        /// </summary>
        private void EndGame()
        {
            _gameTickTimer.Stop();
            _gameTickTimer.IsEnabled = false;
            MessageBox.Show("Tuhle hru jste prohrál", "Hra končí", MessageBoxButton.OK, MessageBoxImage.Information);
            Close();
        }

        /// <summary>
        /// Vytvori herni menu
        /// <returns>Herni menu</returns>
        /// </summary>
        private GameMenu CreateGameMenu()
        {
            GameMenu menu = new GameMenu();
            menu.EndGame += delegate {
                Close();
            };
            menu.ContinueGame += delegate {
                _gameTickTimer.Start();
                menu.Hide();
            };
            menu.NextGame += delegate { SwitchToNextLevel(); };
            menu.PreviousGame += delegate
            {
                InstanceSaver saver = new InstanceSaver();
                SaveFileDialog fileDialog = new SaveFileDialog();
                if (fileDialog.ShowDialog() == true)
                {
                    var file = fileDialog.FileName;
                    try
                    {
                        saver.SaveLevel(_currentLevel, file);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Ukládání levelu {file} selhalo. {ex.Message}", "Ukládání selhalo", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            };
            menu.Closed += delegate {
                _gameTickTimer?.Start();
                _gameMainMenu = null;
            };
            return menu;
        }

        /// <summary>
        /// Graficky vykreslí level
        /// </summary>
        /// <param name="level">Level k vykreslení</param>
        /// <param name="canvas">Canvas pro kreslení</param>
        /// <returns>Herní canvas</returns>
        private void CreatePlayground(Level level, Canvas canvas)
        {
            canvas.Children.Clear();
            Components.DrawLevel(level, canvas);
            Components.DrawPlayer(level.Player, canvas);
            level.Characters.ForEach(character => { Components.DrawEnemy((Enemy) character, canvas); });

            level.Player.Keys = _keys;

            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += delegate
            {
                if (level.Characters.Count <= 0)
                    SwitchToNextLevel();
                if (level.Player.Life <= 0)
                    EndGame();
                level.MoveAll();
                level.ActionAll(timer.Interval.TotalMilliseconds);
                level.Player.Move();
                level.Player.Action(timer.Interval.TotalMilliseconds);
            };
            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.IsEnabled = true;

            _gameTickTimer = timer;
        }

        /// <summary>
        /// Obslužná událost na tlačítka klávesnice
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Argumenty</param>
        private void GameCanvas_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape && _gameMainMenu == null)
            {
                _gameTickTimer.Stop();
                _gameMainMenu = CreateGameMenu();
                _gameMainMenu.ShowDialog();
                _keys.Clear();
            }
            else {
                IsDown = true;
                _keys.Add(e.Key);
            }
        }

        /// <summary>
        /// Obslužná událost na tlačítka klávesnice
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Argumenty</param>
        private void GameCanvas_OnKeyUp(object sender, KeyEventArgs e)
        {
            if(_keys.Contains(e.Key))
                _keys.Remove(e.Key);
            if(_keys.Count == 0)
                IsDown = false;
        }

        /// <summary>
        /// Udalosti po zavření okna
        /// </summary>
        /// <param name="e">Argumenty volání</param>
        protected override void OnClosed(EventArgs e)
        {
            if (_gameTickTimer != null) {
                _gameTickTimer.Stop();
                _gameTickTimer.IsEnabled = false;
            }

            if (_gameMainMenu != null)
            {
                _gameMainMenu.Close();
            }
            _levels.Clear();
            base.OnClosed(e);
        }
    }
}