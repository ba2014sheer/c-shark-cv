﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using Feichero.Game;
using Feichero.Game.FileWorker;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace Feichero
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu
    {
        /// <summary>
        /// Nactene levely ze souboru
        /// </summary>
        private HashSet<Level> _loadedLevels = new HashSet<Level>(new InstanceSaver.LevelComparer());
        /// <summary>
        /// Nástroj pro načítání isntancí levelů
        /// </summary>
        private InstanceSaver _instanceSaver = new InstanceSaver();
        /// <summary>
        /// Dialog pro zobrazení okna a následného načtení fexml souboru
        /// </summary>
        private OpenFileDialog _fileDialog = new OpenFileDialog();
        /// <summary>
        /// Dialog pro zobrazení okna a následné generování levelů
        /// </summary>
        private FolderBrowserDialog _browserDialog = new FolderBrowserDialog();
        /// <summary>
        /// Kontruktor hlavního menu
        /// </summary>
        public MainMenu()
        {
            InitializeComponent();
            _fileDialog.Filter = "Feichero levels|*.fexml";
            _fileDialog.Title = "Načíst level";
        }

        /// <summary>
        /// Po kliknutí na tlačítko nová hra
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Argumenty</param>
        private void NewGameButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_loadedLevels.Count <= 0)
            {
                MessageBox.Show("Nejsou načteny žádné levely. Načtěte je ze složky Levels, která byla dodána s projektem", "Nelze spustit hru", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            MainWindow mainWindow = new MainWindow(_loadedLevels);
            mainWindow.ShowDialog();
            _loadedLevels.Clear();
            LoadedLevelListBox.Items.Clear();
        }

        /// <summary>
        /// Po kliknutí na tlačítko načíst level
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Argumenty</param>
        private void LoadNewLevelButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_fileDialog.ShowDialog() == true)
            {
                var file = _fileDialog.FileName;
                try
                {
                    var level = _instanceSaver.Deserialize(file);
                    if(_loadedLevels.Add(level))
                        LoadedLevelListBox.Items.Add($"{level} - {file}");
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Načítání levelu {file} selhalo. {ex.Message}", "Načtení selhalo", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Po kliknutí na tlačítko Generovat levely
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Argumenty</param>
        private void GenerateNewLevels_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult result = _browserDialog.ShowDialog();
            if( result == System.Windows.Forms.DialogResult.OK )
            {
                try {
                    string folderName =_browserDialog.SelectedPath;
                    Queue<Level> levels = BaseGame.GenerateRandomLevels(folderName, 2);
                    foreach (var level in levels) {
                        _loadedLevels.Add(level);
                        LoadedLevelListBox.Items.Add($"{level} - generováno");
                    }
                    MessageBox.Show("Levely byly uloženy a načteny do hry.", "Generování dokončení", MessageBoxButton.OK, MessageBoxImage.Information);
                } catch (Exception ex) {
                    MessageBox.Show($"Při generování levelů se vyskytla chyba.{ex.Message}", "Generování selhalo", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        
        /// <summary>
        /// Po kliknutí na tlačítko ukončit hru
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Argumenty</param>
        private void QuitGameButton_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
