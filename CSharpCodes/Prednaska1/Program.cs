﻿using System;
using System.Threading.Tasks;

namespace Prednaska1
{
    class Program
    {
        static void Main(string[] args)
        {
            int cislo = 123456;
            long vetsiCislo = 100;
            double desetine = 3.141592;

            char znak = 'Z';
            bool logickaHondnota = true; //IEEE 754
            decimal desitkovyTyp = 0.33m;

            string retezec = "Hello World";
            // String retezec2 = "string 2";

            var var1 = 12345; //Urci se podle inicializovane hodnoty (nelze menit)
            dynamic dynamic = 12345; //Urci se podle inicializovane hodnoty (lze menit)
            dynamic = "Hello";

            int ? nedefinovaneCislo = null; //Nulovatelny typ (? urcuje ze typ muze nabyvat hodnoty null) 

            int c; //vystupni parametr, urcuje se klicovym slovickem out
            if (int.TryParse(Console.ReadLine(), out c)){
                Console.WriteLine($"{c}");
            } else {
                Console.WriteLine("Zjevny nesmysl.");
            }

            int cc = 12345;
            string str = cc.ToString();
            string str1 = 12345.ToString();

            Console.WriteLine($"{cislo} {vetsiCislo}"); //interpolace retezce $
            Console.ReadKey();
        }
    }
}
