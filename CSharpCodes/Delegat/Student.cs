﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegat
{
    enum Fakulta { FES, FF, FEI, FCHT };
    class Student
    {
        public string Jmeno { get; set; }
        public int Cislo { get; set; }
        public Fakulta Fakulta { get; set; }


        public Student(string jmeno, int cislo, Fakulta fakulta) {
            Jmeno = jmeno;
            Cislo = cislo;
            Fakulta = fakulta;
        }

        public override string ToString()
        {
            return $"[{Cislo}] - {Fakulta} : {Jmeno}";
        }
    }
}
