﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegat
{
    class TabulkaStudenta{
        private Studenti Studenti;

        private delegate object StudentKlic(Student st);

        Student NajdiStudenta(StudentKlic stud, object hodnota){
            for (int i = 0; i < Studenti.Length(); ++i) {
                object o = stud(Studenti.Get(i));
                if (o == hodnota) {
                    return Studenti.Get(i);
                }
            }
            return null;
        }


        bool VlozStudenta(StudentKlic stud, object hodnota, Student student) {
            for (int i = 0; i < Studenti.Length(); ++i)
            {
                object o = stud(Studenti.Get(i));
                if (o == hodnota){
                    return false;
                }
            }
            Studenti.Add(student);
            return true;
        }

        bool OdeberStudenta(StudentKlic stud, object hodnota) {
            for (int i = 0; i < Studenti.Length(); ++i){
                object o = stud(Studenti.Get(i));
                if (o == hodnota){
                    Studenti.Remove(Studenti.Get(i));
                    return true;
                }
            }
            return false;
        }
    }
}
