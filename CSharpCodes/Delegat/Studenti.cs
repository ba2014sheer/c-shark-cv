﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegat
{
    class Studenti
    {
        private const int Start = 32;
        private int Current = 0;
        private Student[] StudentiPole = new Student[Start];

        public int Length() {
            return StudentiPole.Length;
        }

        public Student Get(int i) {
            if (i > Current || i < 0 || Current == 0) {
                throw new ArgumentException("Neplatný index");
            }
            return StudentiPole[i];
        }

        public void Add(Student student) {
            if (Current == StudentiPole.Length) {
                Array.Resize(ref StudentiPole, StudentiPole.Length * 2);
            }
            StudentiPole[Current++] = student;
        }

        public void Remove(Student index) {
            for (int i = 0; i < StudentiPole.Length; ++i) {
                if (StudentiPole[i] == index) {
                    StudentiPole[i] = null;
                }
            }
        }


        public void PrintStudenti()
        {
            foreach (var student in StudentiPole)
            {
                if(student != null)
                    Console.WriteLine(student);
            }
        }

        public void SortByName() {
            Array.Sort(StudentiPole, delegate (Student s1, Student s2) {
                return s1.Jmeno.CompareTo(s2.Jmeno);
            });
        }

        public void SortByNumber()
        {
            Array.Sort(StudentiPole, delegate (Student s1, Student s2) {
                return (s1.Cislo > s2.Cislo) ? s1.Cislo : s2.Cislo;
            });
        }

        public void SortByFakulta()
        {
            Array.Sort(StudentiPole, delegate (Student s1, Student s2) {
                return s1.Fakulta.CompareTo(s2.Fakulta);
            });
        }
    }
}
