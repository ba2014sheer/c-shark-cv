﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fei.BaseLib;

namespace Delegat
{
    class Delegat
    {
        static void Main(string[] args)
        {
            Console.WriteLine(MathConvertor.ToDecimal("0111010101"));
            Console.WriteLine(MathConvertor.ToBinary(59854));
            Console.WriteLine(MathConvertor.ToRoman(2900));
            Console.WriteLine(MathConvertor.fromRoman("XC"));

            TabulkaStudenta tabulkaStudenta = new TabulkaStudenta();

            Studenti studenti = new Studenti();

            string line;
            int cislo;
            bool next = false;
            bool end = false;
            do
            {
                do
                {
                    PrintMenu();
                    Console.WriteLine(">");
                    line = Console.ReadLine();
                    if (Int32.TryParse(line, out cislo))
                        next = true;
                    else
                        Console.WriteLine("Zadaný vstup není volba v menu zkuzte to znovu!");
                } while (!next);
                next = false;
                switch (cislo) {
                    case 1:
                        AddStudent(studenti);
                        break;
                    case 2:
                        PrintStudenti(studenti);
                        break;
                    case 3:
                        SortByNumber(studenti);
                        break;
                    case 4:
                        SortByName(studenti);
                        break;
                    case 5:
                        SortByFakulta(studenti);
                        break;
                    case 0:
                        end = true;
                        break;
                }
            } while (!end);            
        }

        static void PrintMenu() {
            Console.WriteLine("-----------MENU-------------");
            Console.WriteLine("1) Načtení studentů z klávesnice");
            Console.WriteLine("2) Výpis studentů na obrazovku");
            Console.WriteLine("3) Seřazení studentů podle čísla");
            Console.WriteLine("4) Seřazení studentů podle jména ");
            Console.WriteLine("5) Seřazení studentů podle fakulty");
            Console.WriteLine("0) Konec programu");
            Console.WriteLine("-----------MENU-------------");
        }

        static void SortByName(Studenti studenti) {
            studenti.SortByName();
        }

        static void SortByNumber(Studenti studenti)
        {
            studenti.SortByNumber();
        }

        static void SortByFakulta(Studenti studenti)
        {
            studenti.SortByFakulta();
        }


        static void PrintStudenti(Studenti studenti) {
            studenti.PrintStudenti();
        }

        static void AddStudent(Studenti studenti) {
            string line, jmeno = null;
            int cislo = 0;
            Fakulta fakulta = Fakulta.FEI;
            bool next = false;

            Console.Write("Zadejte jmeno studenta: ");
            jmeno = Console.ReadLine();

            Console.Write("Zadejte cislo studenta: ");
            do
            {
                line = Console.ReadLine();
                if (Int32.TryParse(line, out cislo))
                    next = true;
                else
                    Console.WriteLine("Zadaný vstup není číslo zkuzte to znovu!");
            } while (!next);

            next = false;
            Console.Write("Zadejte zkratku fakulty (malými písmeny): ");
            do
            {
                line = Console.ReadLine();
                switch (line) {
                    case "fes":
                        fakulta = Fakulta.FES;
                        next = true;
                        break;
                    case "ff":
                        fakulta = Fakulta.FF;
                        next = true;
                        break;
                    case "fei":
                        fakulta = Fakulta.FEI;
                        next = true;
                        break;
                    case "fcht":
                        fakulta = Fakulta.FCHT;
                        next = true;
                        break;
                    default:
                        Console.WriteLine("Fakulta neexistuje. Zkuzte jinou");
                        break;
                }
            } while (!next);
            studenti.Add(new Student(jmeno, cislo, fakulta));
        }
    }
}
