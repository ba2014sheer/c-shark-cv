﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ukol4
{
    class Program
    {
        static void Main(string[] args)
        {
            var random = new Random();
            var r = random.Next(0, 101);

            var count = 10;
            bool again = true;

            while (again) {
                var line = "";
                int number = -1;

                Console.WriteLine("Lets do this! I am thinking some number.");
                while (number != r)
                {
                    if (count == 0)
                    {
                        Console.WriteLine("You lose and I am the winner");
                        return;
                    }

                    Console.WriteLine($"{count--} remaining...");

                    do
                    {
                        Console.Write("Tell me your number: ");
                        line = Console.ReadLine();
                    } while (int.TryParse(line, out number) == false);

                    if (number == r)
                        Console.WriteLine("You win");
                    else if (number > r)
                        Console.WriteLine("Try lower number");
                    else
                        Console.WriteLine("Try upper number");
                }
                Console.WriteLine("What an awesome game! Tell me YES if you wanna play again!");
                line = Console.ReadLine();
                again = (line.Contains("YES"));
            }
            Console.ReadKey();
        }
    }
}
