﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fei
{
    namespace BaseLib
    {
        /// <summary>
        /// Třída obsahuje pokročilé metody pro počítání matematických problémů jako rovnice
        /// </summary>
        /// <remarks>
        /// Třída dokáže vypočítat kořeny kvadratické rovncie a vygenerovat náhodné reálné číslo
        /// </remarks>
        public class ExtraMath
        {
            /// <summary>
            /// Metoda spočítá kořeny jednoduché kvadratické rovnice
            /// </summary>
            /// <param name="a">První člen</param>
            /// <param name="b">Druhý člen</param>
            /// <param name="c">Třetí člen</param>
            /// <param name="x1">Výstupní první kořen</param>
            /// <param name="x2">Výstupní druhý kořen</param>
            /// <returns>Vrací true pokud je výsledek v oboru reálných čísel</returns>
            public static bool KorenyKvadratickeRovnice(double a, double b, double c, out double x1, out double x2)
            {
                x1 = 0.0;
                x2 = 0.0;

                double d = Math.Pow(b, 2) + 4 * a * c;

                if (d < 0)
                    return false;

                x1 = ((-b + Math.Sqrt(d)) / (2 * a));
                x2 = ((-b - Math.Sqrt(d)) / (2 * a));

                return true;
            }

            /// <summary>
            /// Vygeneruje náhodné reálné číslo.
            /// </summary>
            /// <param name="min">Minimální hodnota</param>
            /// <param name="max">Maximální hodnota</param>
            /// <returns>Číslo v intervalu</returns>
            public static double GenerujRandom(double min, double max)
            {
                Random random = new Random();

                if (max <= min)
                {
                    throw new ArgumentException("Minimální hodnota nemůže být větší/stejná jak maximální");
                }

                double vysledek = random.NextDouble();
                return vysledek;
            }
        }
    }
}
