﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fei {
    namespace BaseLib {
        /// <summary>
        /// Třída obsahuje základní metody pro načítání uživatelského vstupu z klávesnice.
        /// Obsahuje metody pro načítání základních datových 
        /// </summary>
        /// <remarks>
        /// Tato třída dokáže načítat následující typy (celé číslo, reálné číslo, znak a řetězec)
        /// </remarks>
        public class Reading {
            /// <summary>
            /// Načte z klávesnice vstup reprezentující datový typ double
            /// </summary>
            /// <param name="banner">
            /// Informační zpráva, která se zobrazí uživateli prostřednictvím konzole.
            /// </param>
            /// <example>
            /// double cislo = Reading.ReadDouble("Zadejte realne cislo: ");
            /// </example>
            /// <exception cref="InvalidCastException">
            /// Vyjímka je vyhozena pokud není vstup validní reálné číslo
            /// </exception>
            /// <returns>
            /// Načtená hodnota daného datového typu
            /// </returns>
            public static double ReadDouble(string banner) {
                Console.Write($"{banner}: ");

                double value = 0.0;
                string line = Console.ReadLine();
                if (Double.TryParse(line, out value) == false) {
                    throw new InvalidCastException("Input value is not double.");
                }

                return value;
            }

            /// <summary>
            /// Načte z klávesnice vstup reprezentující datový typ int
            /// </summary>
            /// <param name="banner">
            /// Informační zpráva, která se zobrazí uživateli prostřednictvím konzole.
            /// </param>
            /// <example>
            /// int cislo = Reading.ReadInt("Zadejte realne cislo: ");
            /// </example>
            /// <exception cref="InvalidCastException">
            /// Vyjímka je vyhozena pokud není vstup validní celé číslo
            /// </exception>
            /// <returns>
            /// Načtená hodnota daného datového typu
            /// </returns>
            public static int ReadInt(string banner)
            {
                Console.Write($"{banner}: ");

                int value = 0;
                string line = Console.ReadLine();
                if (Int32.TryParse(line, out value) == false)
                {
                    throw new InvalidCastException("Input value is not int.");
                }

                return value;
            }

            /// <summary>
            /// Načte z klávesnice vstup reprezentující datový typ char
            /// </summary>
            /// <param name="banner">
            /// Informační zpráva, která se zobrazí uživateli prostřednictvím konzole.
            /// </param>
            /// <example>
            /// double = Reading.ReadChar("Zadejte znak: ");
            /// </example>
            /// <returns>
            /// Načtená hodnota daného datového typu
            /// </returns>
            public static char ReadChar(string banner) {
                Console.Write($"{banner}: ");

                var value = Console.ReadKey();

                return value.KeyChar;
            }

            /// <summary>
            /// Načte z klávesnice vstup reprezentující datový typ string
            /// </summary>
            /// <param name="banner">
            /// Informační zpráva, která se zobrazí uživateli prostřednictvím konzole.
            /// </param>
            /// <example>
            /// double = Reading.ReadString("Zadejte vasi uzasnou myslenku: ");
            /// </example>
            /// <returns>
            /// Načtená hodnota daného datového typu
            /// </returns>
            public static string ReadString(string banner) {
                Console.Write($"{banner}: ");

                string value = Console.ReadLine();
                
                return value;
            }
        }
    }
}
