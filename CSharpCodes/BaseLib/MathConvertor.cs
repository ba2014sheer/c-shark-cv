﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fei
{ 
    namespace BaseLib
{
    /// <summary>
    /// Třída obsahuje metody pro práci s číselnými/prehistorickými soustavami
    /// </summary>
    public class MathConvertor
    {
        /// <summary>
        /// Provede převod do binárního čísla
        /// </summary>
        /// <param name="dec">Hodnota v decimálním čísle</param>
        /// <returns>Binární reprezentaci</returns>
        public static string ToBinary(int dec) {
            int cislo = dec;
            StringBuilder builder = new StringBuilder("");

            for (int i = 0; cislo > 0; ++i) {
                builder.Append(cislo % 2);
                cislo = cislo / 2;
            }

            return builder.ToString();
        }

        /// <summary>
        /// Provede převod do desítokové čísla
        /// </summary>
        /// <param name="binary">Hodnota v bináru</param>
        /// <returns>Binární číslo</returns>
        public static string ToDecimal(string binary) {
            return Convert.ToInt32(binary, 2).ToString();
        }

        /// <summary>
        /// Provede převod do římské číslice
        /// </summary>
        /// <param name="number">Hodnota v desítkové soustavě</param>
        /// <returns>Římská reprezentace</returns>
        public static string ToRoman(int number) {
            if ((number < 0))
                throw new ArgumentOutOfRangeException("Hodnota musí být větší jak nula");
            if (number >= 1000) 
                return "M" + ToRoman(number - 1000);
            if (number >= 900) 
                return "CM" + ToRoman(number - 900);
            if (number >= 500) 
                return "D" + ToRoman(number - 500);
            if (number >= 400) 
                return "CD" + ToRoman(number - 400);
            if (number >= 100) 
                return "C" + ToRoman(number - 100);
            if (number >= 90) 
                return "XC" + ToRoman(number - 90);
            if (number >= 50) 
                return "L" + ToRoman(number - 50);
            if (number >= 40) 
                return "XL" + ToRoman(number - 40);
            if (number >= 10) 
                return "X" + ToRoman(number - 10);
            if (number >= 9) 
                return "IX" + ToRoman(number - 9);
            if (number >= 5) 
                return "V" + ToRoman(number - 5);
            if (number >= 4) 
                return "IV" + ToRoman(number - 4);
            if (number >= 1) 
                return "I" + ToRoman(number - 1);
            return "";
        }

        /// <summary>
        /// Provede převod z římské číslice
        /// </summary>
        /// <param name="roman">Římská číslice</param>
        /// <returns>Desítková číslice</returns>
        public static decimal fromRoman(string roman) {
            Dictionary<char, int > CharValues = new Dictionary<char, int>();
            CharValues.Add('I', 1);
            CharValues.Add('V', 5);
            CharValues.Add('X', 10);
            CharValues.Add('L', 50);
            CharValues.Add('C', 100);
            CharValues.Add('D', 500);
            CharValues.Add('M', 1000);

            int total = 0;
            int lastval = 0;
            for(int i = roman.Length - 1; i >= 0; --i) { 
                int newval = CharValues[roman[i]];

                if (newval < lastval)
                    total -= newval;
                else
                {
                    total += newval;
                    lastval = newval;
                }
            }

            return total;
        }
    }
}
}
