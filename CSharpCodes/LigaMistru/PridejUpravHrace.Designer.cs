﻿namespace LigaMistru
{
    partial class PridejUpravHrace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.JmenoTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.KlubCombobox = new System.Windows.Forms.ComboBox();
            this.GolyTextBox = new System.Windows.Forms.TextBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.StornoButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.68805F));
            this.tableLayoutPanel1.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.31195F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.JmenoTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.KlubCombobox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.GolyTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.OkButton, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.StornoButton, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.29578F));
            this.tableLayoutPanel1.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.70422F));
            this.tableLayoutPanel1.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel1.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(457, 229);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(214, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jméno";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // JmenoTextBox
            // 
            this.JmenoTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.JmenoTextBox.Location = new System.Drawing.Point(226, 5);
            this.JmenoTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.JmenoTextBox.Name = "JmenoTextBox";
            this.JmenoTextBox.Size = new System.Drawing.Size(227, 27);
            this.JmenoTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 55);
            this.label2.TabIndex = 2;
            this.label2.Text = "Klub";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 109);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 65);
            this.label3.TabIndex = 3;
            this.label3.Text = "Góly";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // KlubCombobox
            // 
            this.KlubCombobox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KlubCombobox.FormattingEnabled = true;
            this.KlubCombobox.Location = new System.Drawing.Point(226, 59);
            this.KlubCombobox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.KlubCombobox.Name = "KlubCombobox";
            this.KlubCombobox.Size = new System.Drawing.Size(227, 28);
            this.KlubCombobox.TabIndex = 4;
            // 
            // GolyTextBox
            // 
            this.GolyTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GolyTextBox.Location = new System.Drawing.Point(226, 114);
            this.GolyTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GolyTextBox.Name = "GolyTextBox";
            this.GolyTextBox.Size = new System.Drawing.Size(227, 27);
            this.GolyTextBox.TabIndex = 5;
            this.GolyTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GolyTextBox_KeyPress);
            // 
            // OkButton
            // 
            this.OkButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.OkButton.Location = new System.Drawing.Point(4, 179);
            this.OkButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(214, 35);
            this.OkButton.TabIndex = 6;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // StornoButton
            // 
            this.StornoButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.StornoButton.Location = new System.Drawing.Point(226, 179);
            this.StornoButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.StornoButton.Name = "StornoButton";
            this.StornoButton.Size = new System.Drawing.Size(227, 35);
            this.StornoButton.TabIndex = 7;
            this.StornoButton.Text = "Storno";
            this.StornoButton.UseVisualStyleBackColor = true;
            this.StornoButton.Click += new System.EventHandler(this.StornoButton_Click);
            // 
            // PridejUpravHrace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 229);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "PridejUpravHrace";
            this.Text = "Hrac";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox JmenoTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox KlubCombobox;
        private System.Windows.Forms.TextBox GolyTextBox;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button StornoButton;
    }
}