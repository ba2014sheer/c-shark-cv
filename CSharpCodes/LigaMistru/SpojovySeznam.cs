﻿using System;
using System.Collections;
using System.Globalization;
using System.Management.Instrumentation;

namespace LigaMistru
{
    public class SpojovySeznam : IEnumerable, ICollection, IList{
        private class PrvekSeznamu {
            public object Data { get; set; }
            public PrvekSeznamu Dalsi;
            public PrvekSeznamu Predchozi;

            public PrvekSeznamu(object data)
            {
                Data = data;
            }

            public PrvekSeznamu(object data, PrvekSeznamu dalsi, PrvekSeznamu predchozi)
            {
                Data = data;
                Dalsi = dalsi;
                Predchozi = predchozi;
            }
        }

        private class HraciEnumerator : IEnumerator
        {
            private PrvekSeznamu _next;
            private PrvekSeznamu _first;
            private bool _started = true;
            public HraciEnumerator(PrvekSeznamu first)
            {
                _first = first;
            }

            public bool MoveNext()
            {
                if (_started)
                {
                    _next = _first;
                    _started = false;
                    return true;
                }
                _next = _next.Dalsi;
                
                if (_next == null)
                    return false;

                return true;
            }

            public void Reset()
            {
                _started = true;
                _next = null;
            }

            public object Current { get => _next.Data; }
        }

        private PrvekSeznamu _prvni;
        private PrvekSeznamu _posledni;

        public IEnumerator GetEnumerator()
        {
            return new HraciEnumerator(_prvni);
        }

        public void CopyTo(Array array, int index)
        {
            if(array == null)
                throw new NullReferenceException();
            if (index >= Count || index < 0)
                throw new IndexOutOfRangeException();

            int i = 0;
            foreach (var hrac in this)
            {
                array.SetValue(hrac, i++);
            }
        }

        public int Count { get; protected set; }
        public object SyncRoot => this;
        public bool IsSynchronized => false;

        public int Add(object value)
        {
            PrvekSeznamu prvekSeznamu = new PrvekSeznamu(value);
            prvekSeznamu.Dalsi = _prvni;
            _prvni = prvekSeznamu;

            if (_prvni.Dalsi != null)
                _prvni.Dalsi.Predchozi = prvekSeznamu;
            
            if (_posledni == null)
                _posledni = _prvni;
            
            Count++;
            return 1;
        }

        public int AddToEnd(object value)
        {
            PrvekSeznamu prvekSeznamu = new PrvekSeznamu(value);
            prvekSeznamu.Predchozi = _posledni;
            _posledni = prvekSeznamu;

            if (_posledni.Predchozi != null)
                _posledni.Predchozi.Dalsi = prvekSeznamu;
            
            if (_prvni == null)
                _prvni = _posledni;

            Count++;
            return 1;
        }

        public bool Contains(object value)
        {
            PrvekSeznamu next = _prvni;
            while (next != null)
            {
                if (next.Data == value) 
                    return true;
                next = next.Dalsi;
            }

            return false;
        }

        public void Clear()
        {
            _prvni = null;
            _posledni = null;
            Count = 0;
        }

        public int IndexOf(object value)
        {
            PrvekSeznamu next = _prvni;
            for (var i = 0; next != null; ++i)
            {
                if (next.Data == value)
                    return i;
                next = next.Dalsi;
            }

            return -1;
        }

        public void Insert(int index, object value)
        {
            if (index >= Count || index < 0)
                throw new IndexOutOfRangeException();

            if (index == Count - 1) {
                AddToEnd(value);
                return;
            }

            if (index == 0) {
                Add(value);
                return;
            }
            
            PrvekSeznamu next = _prvni;
            for (var i = 0;  next != null; ++i)
            {
                if (i == index)
                {
                    PrvekSeznamu prvekSeznamu = new PrvekSeznamu(value, next, next.Predchozi);
                    next.Predchozi.Dalsi = prvekSeznamu;
                    next.Predchozi = prvekSeznamu;
                    break;
                }
                next = next.Dalsi;
            }
        }

        private PrvekSeznamu AtIndex(int index)
        {
            if (index >= Count || index < 0)
                throw new IndexOutOfRangeException();
            
            PrvekSeznamu next = _prvni;
            for (var i = 0;  next != null; ++i) {
                if (index == i)
                {
                    return next;
                }
                next = next.Dalsi;
            }
            
            throw new IndexOutOfRangeException();
        }

        private void SetToIndex(int index, object data)
        {
            PrvekSeznamu prvekSeznamu = AtIndex(index);
            prvekSeznamu.Data = data;
        }

        public void Remove(object value)
        {
            RemoveAt(IndexOf(value));
        }

        public void Remove()
        {
            if(Count == 0)
                throw new IndexOutOfRangeException();
             
            if (_prvni.Dalsi != null)
                _prvni.Dalsi.Predchozi = null;
            
            if (_prvni == _posledni)
                _posledni = null;
            
            _prvni = _prvni.Dalsi;

            Count--;
        }
        
        public void RemoveEnd()
        {
            if(Count == 0)
                throw new IndexOutOfRangeException();
            
            if (_prvni.Predchozi!= null)
                _prvni.Predchozi.Dalsi = null;
            
            if (_prvni == _posledni)
                _prvni = null;

            _posledni = _posledni.Predchozi;

            Count--;
        }

        public void RemoveAt(int index)
        {
            if (index == Count - 1) {
                RemoveEnd();
                return;
            }

            if (index == 0)
            {
                Remove();
                return;
            }

            PrvekSeznamu next = AtIndex(index);
            next.Predchozi.Dalsi = next.Dalsi;
            next.Dalsi.Predchozi = next.Predchozi;
            Count--;
        }

        public object this[int index]
        {
            get => AtIndex(index).Data;
            set => SetToIndex(index, value);
        }

        public bool IsReadOnly { get => false; }
        public bool IsFixedSize { get => false;  }
    }
}