﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMistru
{
    public partial class ZobrazKluby : Form
    {
        public ZobrazKluby(FotbalovyKlub[] kluby, int goly)
        {
            InitializeComponent();
            foreach (var klub in kluby)
                KlubyListView.Items.Add(FotbalovyKlubInfo.DejNazev(klub));
            PocetGoluTextBox.Text = goly + "";
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
