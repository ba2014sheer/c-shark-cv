﻿namespace LigaMistru
{
    partial class VyberKluby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.mainListView = new System.Windows.Forms.ListView();
            this.ulozButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.mainListView);
            this.flowLayoutPanel1.Controls.Add(this.ulozButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(433, 692);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // mainListView
            // 
            this.mainListView.HideSelection = false;
            this.mainListView.Location = new System.Drawing.Point(4, 5);
            this.mainListView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mainListView.Name = "mainListView";
            this.mainListView.Size = new System.Drawing.Size(428, 610);
            this.mainListView.TabIndex = 0;
            this.mainListView.UseCompatibleStateImageBehavior = false;
            this.mainListView.View = System.Windows.Forms.View.List;
            // 
            // ulozButton
            // 
            this.ulozButton.Location = new System.Drawing.Point(4, 625);
            this.ulozButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ulozButton.Name = "ulozButton";
            this.ulozButton.Size = new System.Drawing.Size(429, 48);
            this.ulozButton.TabIndex = 1;
            this.ulozButton.Text = "Ulož Kluby";
            this.ulozButton.UseVisualStyleBackColor = true;
            this.ulozButton.Click += new System.EventHandler(this.SaveKlubsClick);
            // 
            // VyberKluby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 692);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VyberKluby";
            this.Text = "VyberKluby";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ListView mainListView;
        private System.Windows.Forms.Button ulozButton;
    }
}