﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static LigaMistru.Hraci;

namespace LigaMistru
{
    public partial class ZobrazHrace : Form
    {
        private Hraci hraci = new Hraci();

        private PocetHracuChangeEventHandler handler;

        public ZobrazHrace()
        {
            InitializeComponent();

            MainDataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            hraci.PocetZmenen += delegate (int pocetHracu) {
                ObservableCollection<Hrac> data = new ObservableCollection<Hrac>();
                for (var i = 0; i < hraci.Pocet; ++i)
                    data.Add(hraci[i]);

                MainDataGrid.DataSource = data;
                MainDataGrid.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                MainDataGrid.Columns[0].HeaderText = "Jméno";
                MainDataGrid.Columns[1].HeaderText = "Klub";
                MainDataGrid.Columns[2].HeaderText = "Góly";
            };

            handler = delegate (int pocetHracu){
                InfoListBox.Items.Add($"Proběhla změna počtu nebo informace o hráčích {pocetHracu}");
            };
        }

        private void KonecButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PridejButton_Click(object sender, EventArgs e)
        {
            PridejUpravHrace pridejHrace = new PridejUpravHrace(hraci);
            pridejHrace.Show(this);
        }

        private void VymazButton_Click(object sender, EventArgs e)
        {
            if (MainDataGrid.CurrentRow.DataBoundItem == null || hraci.Pocet <= 0)
                return;
            Hrac hrac = (Hrac)MainDataGrid.CurrentRow.DataBoundItem;
            for (var i = 0; i < hraci.Pocet; ++i)
                if (hraci[i] == hrac)
                    hraci.Vymaz(i);
        }

        private void UpravitButton_Click(object sender, EventArgs e)
        {
            if (MainDataGrid.CurrentRow.DataBoundItem == null)
                return;
            Hrac hrac = (Hrac)MainDataGrid.CurrentRow.DataBoundItem;
            PridejUpravHrace pridejHrace = new PridejUpravHrace(hraci, hrac);
            pridejHrace.Show(this);
        }

        private void NejlepsiButton_Click(object sender, EventArgs e)
        {
            int goly = 0;
            FotbalovyKlub[] kluby = null;

            hraci.NajdiNejlepsiKluby(out kluby, out goly);
            ZobrazKluby zobrazKluby = new ZobrazKluby(kluby, goly);
            zobrazKluby.Show(this);
        }

        private void RegistrovatButton_Click(object sender, EventArgs e)
        {
            hraci.PocetZmenen += handler;
        }

        private void ZrusitButton_Click(object sender, EventArgs e)
        {
            hraci.PocetZmenen -= handler;
        }

        private void ulozClick(object sender, EventArgs e)
        {
           VyberKluby vyberKluby = new VyberKluby(hraci);
           vyberKluby.Show(this);
        }

        private void Nacti_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Textový soubor|*.txt";
            openFileDialog.Title = "Uložit Hráče";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var filePath = openFileDialog.FileName;
                hraci.FromFile(filePath, InfoListBox);
            }
        }
    }
}
