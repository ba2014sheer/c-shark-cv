﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMistru
{
    public enum FotbalovyKlub { 
        None, FCPorto, Arsenal, RealMadrid, Chelsea, Barcelona
    };

    public static class FotbalovyKlubInfo {
        public static readonly int Pocet = 6;
        public static string DejNazev(FotbalovyKlub klub) {
            switch (klub) {
                case FotbalovyKlub.None:
                    return "None";
                case FotbalovyKlub.FCPorto:
                    return "FCPorto";
                case FotbalovyKlub.Arsenal:
                    return "Arsenal";
                case FotbalovyKlub.RealMadrid:
                    return "RealMadrid";
                case FotbalovyKlub.Chelsea:
                    return "Chelsea";
                case FotbalovyKlub.Barcelona:
                    return "Barcelona";
            }
            return null;
        }
        public static FotbalovyKlub DejKlub(string nazev) {
            switch (nazev) {
                
                case "FCPorto":
                    return FotbalovyKlub.FCPorto;
                case "Arsenal":
                    return FotbalovyKlub.Arsenal;
                case "RealMadrid":
                        return FotbalovyKlub.RealMadrid;
                case "Chelsea":
                    return FotbalovyKlub.Chelsea;
                case "Barcelona":    
                    return FotbalovyKlub.Barcelona;
                case "None":  
                    return FotbalovyKlub.None;
                default:
                    throw new InvalidEnumArgumentException();
            }
        }

        public static readonly object[] kluby = new object[] { FotbalovyKlub.Arsenal, FotbalovyKlub.Barcelona, FotbalovyKlub.Chelsea, FotbalovyKlub.FCPorto, FotbalovyKlub.None, FotbalovyKlub.RealMadrid };
    }

    public class Hrac { 
        public string Jmeno { get; set; }
        public FotbalovyKlub Klub { get; set; }
        public int GolPocet { get; set; }

        public Hrac(string jmeno, FotbalovyKlub klub, int golPocet)
        {
            Jmeno = jmeno ?? throw new ArgumentNullException(nameof(jmeno));
            Klub = klub;
            GolPocet = golPocet;
        }

        public override string ToString()
        {
            return $"{Jmeno}\n{Klub}\n{GolPocet}";
        }

        public override bool Equals(object obj)
        {
            return String.Equals(ToString(), obj.ToString());
        }
    }

    public class Hraci {
        private SpojovySeznam hraci = new SpojovySeznam();

        public delegate void PocetHracuChangeEventHandler(int pocetHracu);

        public event PocetHracuChangeEventHandler PocetZmenen;

        public int Pocet
        {
            get => hraci.Count;
        }
        public void Vymaz(int index)
        {
            int puvodniPocet = hraci.Count;
            
            var h = hraci[index];
            hraci.RemoveAt(index);

            OnPocetHracuChanged(puvodniPocet);
        }
        public void Pridej(Hrac h)
        {
            if (hraci.Contains(h))
                return;
            int puvodniPocet = hraci.Count;
            hraci.Add(h);
            OnPocetHracuChanged(puvodniPocet);
        }
        public Hrac this[int index] {
            get => (Hrac) hraci[index];
        }
        public void NajdiNejlepsiKluby(out FotbalovyKlub[] kluby, out int golPocet) {
            kluby = new FotbalovyKlub[0];
            golPocet = 0;

            //Vypocitej celkovy pocet golu pro vsechny kluby a ulož to do mapy
            var golyKlubu = new Dictionary<FotbalovyKlub, int>();
            foreach (var hrac in hraci) {
                if (!golyKlubu.ContainsKey(((Hrac)hrac).Klub)) 
                    golyKlubu.Add(((Hrac)hrac).Klub, ((Hrac)hrac).GolPocet);
                else 
                    golyKlubu[((Hrac)hrac).Klub] = golyKlubu[((Hrac)hrac).Klub] + ((Hrac)hrac).GolPocet;

                if (golyKlubu[((Hrac)hrac).Klub] >= golPocet) //Nasel jsem novy rekord
                    golPocet = golyKlubu[((Hrac)hrac).Klub];
            }

            //Projdi vsechny kluby a pokud jejich vysledek je rovny nejlepsimu prijde ho do listu
            List<FotbalovyKlub> nejlepsiKluby = new List<FotbalovyKlub>();
            foreach (FotbalovyKlub klub in golyKlubu.Keys)
                if (golyKlubu[klub] == golPocet)
                    nejlepsiKluby.Add(klub);
            
            //Preved list do pole
            kluby = nejlepsiKluby.ToArray();
        }

        public HashSet<FotbalovyKlub> VratVsechnyKluby()
        {
            HashSet<FotbalovyKlub> kluby = new HashSet<FotbalovyKlub>();
            foreach (var hrac in hraci)
            {
                kluby.Add(((Hrac) hrac).Klub);
            }

            return kluby;
        }

        private void OnPocetHracuChanged(int puvodniPocet) {
            PocetHracuChangeEventHandler eventHandler = PocetZmenen;
            if(eventHandler != null){
                eventHandler(puvodniPocet);
            }
        }

        public void Refresh() {
            OnPocetHracuChanged(hraci.Count);
        }

        public void ToFile(string path, ICollection<FotbalovyKlub> kluby)
        {
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.OpenOrCreate), Encoding.UTF8);
            foreach (var klub in kluby)
                foreach (var hrac in hraci)
                    if((((Hrac)hrac).Klub) == klub)
                        writer.WriteLine(hrac.ToString());
            writer.Flush();
            writer.Close();
        }

        public void FromFile(string path, ListBox box)
        {
            StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), Encoding.UTF8);
            while (!reader.EndOfStream) {
                try {
                    string jmeno = reader.ReadLine();
                    FotbalovyKlub klub = FotbalovyKlubInfo.DejKlub(reader.ReadLine());
                    int goly = 0;
                    if (Int32.TryParse(reader.ReadLine(), out goly) == false)
                        continue;
                    var hrac = new Hrac(jmeno, klub, goly);
                    Pridej(hrac);
                }
                catch (Exception e) {
                    box.Items.Add(e.ToString());
                    throw;
                }
            }
        }
    }
}
