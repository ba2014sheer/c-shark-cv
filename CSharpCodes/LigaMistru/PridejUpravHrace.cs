﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMistru
{
    public partial class PridejUpravHrace : Form
    {
        private Hraci hraci;
        private Hrac hrac;
        public PridejUpravHrace(Hraci hraci)
        {
            InitializeComponent();
            KlubCombobox.Items.AddRange(FotbalovyKlubInfo.kluby);
            KlubCombobox.SelectedItem = FotbalovyKlub.None;
            GolyTextBox.Text = "0";
            this.hraci = hraci ?? throw new ArgumentNullException(nameof(hraci));
        }

        public PridejUpravHrace(Hraci hraci, Hrac hrac) {
            InitializeComponent();
            KlubCombobox.Items.AddRange(FotbalovyKlubInfo.kluby);

            JmenoTextBox.Text = hrac.Jmeno;
            KlubCombobox.SelectedItem = hrac.Klub;
            GolyTextBox.Text = hrac.GolPocet + "";
            this.hrac = hrac;
            this.hraci = hraci ?? throw new ArgumentNullException(nameof(hraci));
        }

        private void GolyTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&(e.KeyChar != '.')){
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1)){
                e.Handled = true;
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (JmenoTextBox.Text.Length == 0) {
                MessageBox.Show("Jméno musí obsahovat hodnotu", "Jméno není vyplněno", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (KlubCombobox.SelectedItem == null) {
                MessageBox.Show("Klub musí obsahovat hodnotu", "Klub není vyplněn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (GolyTextBox.Text.Length == 0) {
                MessageBox.Show("Góly musí obsahovat hodnotu", "Počet gólů není zadán", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (hrac != null) {
                hrac.Jmeno = JmenoTextBox.Text;
                hrac.Klub = (FotbalovyKlub)KlubCombobox.SelectedItem;
                hrac.GolPocet = Int32.Parse(GolyTextBox.Text);
                hraci.Refresh();
            } else {
                Hrac hrac = new Hrac(JmenoTextBox.Text, (FotbalovyKlub)KlubCombobox.SelectedItem, Int32.Parse(GolyTextBox.Text));
                hraci.Pridej(hrac);
            }
            Close();
        }

        private void StornoButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
