﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMistru
{
    public partial class VyberKluby : Form
    {
        private Hraci _hraci;

        public VyberKluby(Hraci hraci)
        {
            _hraci = hraci;
            InitializeComponent();
            foreach (var klub in _hraci.VratVsechnyKluby())
                mainListView.Items.Add(klub.ToString());
        }

        public VyberKluby()
        {
            InitializeComponent();
            foreach (var klub in _hraci.VratVsechnyKluby())
                mainListView.Items.Add(klub.ToString());
        }

        private void SaveKlubsClick(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Textový soubor|*.txt";
            saveFileDialog1.Title = "Uložit Hráče";
            saveFileDialog1.ShowDialog();

            IList<FotbalovyKlub> klubs = new List<FotbalovyKlub>();
            var kluby = mainListView.SelectedItems;
            foreach (ListViewItem klub in kluby)
                klubs.Add(FotbalovyKlubInfo.DejKlub( klub.Text ));
            
            
            if (saveFileDialog1.FileName != "")
            {
                _hraci.ToFile(saveFileDialog1.FileName, klubs);
            }
        }
    }
}
