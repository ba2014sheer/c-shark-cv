﻿namespace LigaMistru
{
    partial class ZobrazHrace
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.MainDataGrid = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.PridejButton = new System.Windows.Forms.Button();
            this.VymazButton = new System.Windows.Forms.Button();
            this.UpravitButton = new System.Windows.Forms.Button();
            this.NejlepsiButton = new System.Windows.Forms.Button();
            this.RegistrovatButton = new System.Windows.Forms.Button();
            this.ZrusitButton = new System.Windows.Forms.Button();
            this.KonecButton = new System.Windows.Forms.Button();
            this.ulozButton = new System.Windows.Forms.Button();
            this.Nacti = new System.Windows.Forms.Button();
            this.InfoListBox = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.MainDataGrid)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.InfoListBox);
            this.splitContainer1.Size = new System.Drawing.Size(934, 519);
            this.splitContainer1.SplitterDistance = 408;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.CausesValidation = false;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.MainDataGrid);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.flowLayoutPanel1);
            this.splitContainer2.Panel2MinSize = 50;
            this.splitContainer2.Size = new System.Drawing.Size(934, 408);
            this.splitContainer2.SplitterDistance = 805;
            this.splitContainer2.TabIndex = 0;
            // 
            // MainDataGrid
            // 
            this.MainDataGrid.AllowUserToAddRows = false;
            this.MainDataGrid.AllowUserToDeleteRows = false;
            this.MainDataGrid.AllowUserToOrderColumns = true;
            this.MainDataGrid.ColumnHeadersHeightSizeMode =
                System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MainDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainDataGrid.Location = new System.Drawing.Point(0, 0);
            this.MainDataGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MainDataGrid.Name = "MainDataGrid";
            this.MainDataGrid.ReadOnly = true;
            this.MainDataGrid.RowHeadersWidth = 51;
            this.MainDataGrid.Size = new System.Drawing.Size(805, 408);
            this.MainDataGrid.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.PridejButton);
            this.flowLayoutPanel1.Controls.Add(this.VymazButton);
            this.flowLayoutPanel1.Controls.Add(this.UpravitButton);
            this.flowLayoutPanel1.Controls.Add(this.NejlepsiButton);
            this.flowLayoutPanel1.Controls.Add(this.RegistrovatButton);
            this.flowLayoutPanel1.Controls.Add(this.ZrusitButton);
            this.flowLayoutPanel1.Controls.Add(this.KonecButton);
            this.flowLayoutPanel1.Controls.Add(this.ulozButton);
            this.flowLayoutPanel1.Controls.Add(this.Nacti);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(125, 408);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // PridejButton
            // 
            this.PridejButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.PridejButton.Location = new System.Drawing.Point(4, 4);
            this.PridejButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PridejButton.Name = "PridejButton";
            this.PridejButton.Size = new System.Drawing.Size(116, 26);
            this.PridejButton.TabIndex = 0;
            this.PridejButton.Text = "Přidej";
            this.PridejButton.UseVisualStyleBackColor = true;
            this.PridejButton.Click += new System.EventHandler(this.PridejButton_Click);
            // 
            // VymazButton
            // 
            this.VymazButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.VymazButton.Location = new System.Drawing.Point(4, 38);
            this.VymazButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.VymazButton.Name = "VymazButton";
            this.VymazButton.Size = new System.Drawing.Size(116, 26);
            this.VymazButton.TabIndex = 1;
            this.VymazButton.Text = "Vymaž";
            this.VymazButton.UseVisualStyleBackColor = true;
            this.VymazButton.Click += new System.EventHandler(this.VymazButton_Click);
            // 
            // UpravitButton
            // 
            this.UpravitButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.UpravitButton.Location = new System.Drawing.Point(4, 72);
            this.UpravitButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.UpravitButton.Name = "UpravitButton";
            this.UpravitButton.Size = new System.Drawing.Size(116, 26);
            this.UpravitButton.TabIndex = 2;
            this.UpravitButton.Text = "Upravit";
            this.UpravitButton.UseVisualStyleBackColor = true;
            this.UpravitButton.Click += new System.EventHandler(this.UpravitButton_Click);
            // 
            // NejlepsiButton
            // 
            this.NejlepsiButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.NejlepsiButton.Location = new System.Drawing.Point(4, 106);
            this.NejlepsiButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NejlepsiButton.Name = "NejlepsiButton";
            this.NejlepsiButton.Size = new System.Drawing.Size(116, 26);
            this.NejlepsiButton.TabIndex = 3;
            this.NejlepsiButton.Text = "Nejlepší";
            this.NejlepsiButton.UseVisualStyleBackColor = true;
            this.NejlepsiButton.Click += new System.EventHandler(this.NejlepsiButton_Click);
            // 
            // RegistrovatButton
            // 
            this.RegistrovatButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.RegistrovatButton.Location = new System.Drawing.Point(4, 140);
            this.RegistrovatButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RegistrovatButton.Name = "RegistrovatButton";
            this.RegistrovatButton.Size = new System.Drawing.Size(116, 26);
            this.RegistrovatButton.TabIndex = 4;
            this.RegistrovatButton.Text = "Registrovat";
            this.RegistrovatButton.UseVisualStyleBackColor = true;
            this.RegistrovatButton.Click += new System.EventHandler(this.RegistrovatButton_Click);
            // 
            // ZrusitButton
            // 
            this.ZrusitButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.ZrusitButton.Location = new System.Drawing.Point(4, 174);
            this.ZrusitButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ZrusitButton.Name = "ZrusitButton";
            this.ZrusitButton.Size = new System.Drawing.Size(116, 26);
            this.ZrusitButton.TabIndex = 5;
            this.ZrusitButton.Text = "Zrušit";
            this.ZrusitButton.UseVisualStyleBackColor = true;
            this.ZrusitButton.Click += new System.EventHandler(this.ZrusitButton_Click);
            // 
            // KonecButton
            // 
            this.KonecButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.KonecButton.Location = new System.Drawing.Point(4, 208);
            this.KonecButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.KonecButton.Name = "KonecButton";
            this.KonecButton.Size = new System.Drawing.Size(116, 26);
            this.KonecButton.TabIndex = 6;
            this.KonecButton.Text = "Konec";
            this.KonecButton.UseVisualStyleBackColor = true;
            this.KonecButton.Click += new System.EventHandler(this.KonecButton_Click);
            // 
            // ulozButton
            // 
            this.ulozButton.Location = new System.Drawing.Point(4, 242);
            this.ulozButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ulozButton.Name = "ulozButton";
            this.ulozButton.Size = new System.Drawing.Size(116, 26);
            this.ulozButton.TabIndex = 7;
            this.ulozButton.Text = "Ulož";
            this.ulozButton.UseVisualStyleBackColor = true;
            this.ulozButton.Click += new System.EventHandler(this.ulozClick);
            // 
            // Nacti
            // 
            this.Nacti.Location = new System.Drawing.Point(4, 276);
            this.Nacti.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Nacti.Name = "Nacti";
            this.Nacti.Size = new System.Drawing.Size(116, 26);
            this.Nacti.TabIndex = 8;
            this.Nacti.Text = "Načti";
            this.Nacti.UseVisualStyleBackColor = true;
            this.Nacti.Click += new System.EventHandler(this.Nacti_Click);
            // 
            // InfoListBox
            // 
            this.InfoListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoListBox.FormattingEnabled = true;
            this.InfoListBox.ItemHeight = 15;
            this.InfoListBox.Location = new System.Drawing.Point(0, 0);
            this.InfoListBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InfoListBox.Name = "InfoListBox";
            this.InfoListBox.Size = new System.Drawing.Size(934, 106);
            this.InfoListBox.TabIndex = 0;
            // 
            // ZobrazHrace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 519);
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ZobrazHrace";
            this.Text = "Liga Mistrů";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.MainDataGrid)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button PridejButton;
        private System.Windows.Forms.Button VymazButton;
        private System.Windows.Forms.Button UpravitButton;
        private System.Windows.Forms.Button NejlepsiButton;
        private System.Windows.Forms.Button RegistrovatButton;
        private System.Windows.Forms.Button ZrusitButton;
        private System.Windows.Forms.Button KonecButton;
        private System.Windows.Forms.DataGridView MainDataGrid;
        private System.Windows.Forms.ListBox InfoListBox;
        private System.Windows.Forms.Button ulozButton;
        private System.Windows.Forms.Button Nacti;
    }
}

