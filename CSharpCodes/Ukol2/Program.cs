﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ukol2
{
    class Program
    {
        static void Main(string[] args)
        {
            char znak = 'A';
            do {
                Console.Write($"{znak} ");
                znak++;
            } while (znak != ('Z'+1));

            Console.WriteLine();
            znak = 'A';
            while (znak != 'Z' + 1) {
                Console.Write($"{znak} ");
                znak++;
            }

            Console.WriteLine();
            for (var i = 'A'; i != ('Z' + 1); i++) {
                Console.Write($"{i} ");
            }

            Console.ReadKey();
        }
    }
}
