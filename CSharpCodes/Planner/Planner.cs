﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planner
{
    /// <summary>
    /// Planovac udalosti
    /// </summary>
    public class Planner
    {
        /// <summary>
        /// Pocet praci
        /// </summary>
        public static int MaxJobs = 100;
        /// <summary>
        /// Pocet stroju
        /// </summary>
        public static int MaxMachines = 8;
        /// <summary>
        /// Kalendar
        /// </summary>
        private Job[,] _calendar = new Job[MaxMachines, MaxJobs];
        /// <summary>
        /// Joby
        /// </summary>
        private List<Job> _jobs;
        /// <summary>
        /// Vytvori plan
        /// </summary>
        /// <param name="jobs"> plan </param>
        public Planner(List<Job> jobs)
        {
            FirstFit(jobs);
        }

        /// <summary>
        /// Metoda pro prvni algoritmus
        /// </summary>
        /// <param name="jobs">joby pro vlozeni</param>
        public void FirstFit(List<Job> jobs)
        {
            _jobs = jobs;

            jobs.Sort((job, job1) => job.CreateDate - job1.CreateDate);
            jobs.ForEach(job => { //Pro kazdy job
                int withoutClean = 0; //Tato promenna bude indikovat kolik casu maji stroje v listu
                //Vytvorim si list, ktery bude obsahovat volne stroje
                List<int> free = new List<int>(MaxMachines);
                for (int i = 0; i < MaxMachines; i++)
                    free.Add(i); //Naplnim ho

                for (int i = 0; i < MaxJobs; i++) { //prochazim casove sloty po sloupcich
                    List<int> toFree = new List<int>();
                    for (int j = 0; j < MaxMachines; j++) //Projdu vsechny stroje v danem sloupci
                        if (_calendar[j, i] != null) //Pokud stroj nema cas 
                            free.Remove(j); //odeberu ho z listu volnych stroju
                        else if(!free.Contains(j))
                            toFree.Add(j);
                    
                    withoutClean++; //Inkrementuju pocet prochazeni
                    if (free.Count < job.Machines) { //Pokud nema dostatecny pocet stroju cas
                        if (free.Count + toFree.Count >= job.Machines) {
                            withoutClean = 1;
                            free.AddRange(toFree);
                        } else {
                            withoutClean = 0;
                            free.Clear(); //Vycistim list
                            for (int k = 0; k < MaxMachines; k++) // a znova ho naplnim vsemi stroji
                                free.Add(k);
                            //A jdu zkoumat dalsi sloupecek v poradi (planu)
                        }
                    }

                    if (withoutClean == job.Time) { //Pokud jsem nasel dostatecny pocet stroju co ma cas
                        var t = i - withoutClean + 1;
                        AddToCalendar((t < 0)? 0 : t, free, job); //Pridam Job do kalendare
                        break; //koncim vnejsi for cyklus, jdu zkoumat dalsi job v poradi
                    }
                }
            });
        }

        public override string ToString()
        {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < _calendar.GetLength(0); i++)
            {
                output.Append($"0{i} ");
                for (int j = 0; j < _calendar.GetLength(1); j++) {
                    output.Append((_calendar[i, j] != null)? _calendar[i, j].Id.ToString() : "-");
                }
                output.Append("\n");
            }
            output.Append("\n");
            _jobs.ForEach(job => { output.Append(job + "\n"); });

            output.Append("\nNejdelsi ulohy:\n");
            var longest = FindTheLongest();
            for (var i = 0; i < longest.Count; i++)
                output.Append($"0{i} machine: {longest[i]}\n");

            return output.ToString();
        }

        /// <summary>
        /// Najde nejdelší trvání pokud budeme potřebovat N strojů
        /// </summary>
        /// <returns>List kde každý index +1 představuje počet strojů a hodnota jakou nejdelsi udalost zvladnou</returns>
        public List<int> FindTheLongest()
        {
            int max = Int32.MinValue;
            for (int i = 0; i < _calendar.GetLength(0); i++) {
                int duration = 0;
                for (int j = 0; j < _calendar.GetLength(1); j++) {
                    if (_calendar[i, j] == null)
                        duration++;
                    else {
                        if (duration > max) max = duration;
                        duration = 0;
                    }
                }
                if (duration > max) max = duration;
            }

            List<int> longest = new List<int>();
            for (int j = 1; j <= MaxMachines; j++) {
                for (int i = max; i >= 0; i--)
                {
                    if (WillFilledIn(i, j) || max == 0) {
                        longest.Add(i);
                        break;
                    }
                }
            }
            return longest;
        }
        
        /// <summary>
        /// Zjistí jestli se událost vejde do kalendáře
        /// </summary>
        /// <param name="duration">délka události</param>
        /// <param name="machines">potřebný počet strojů</param>
        /// <returns></returns>
        private bool WillFilledIn(int duration, int machines)
        {
            int withoutClean = 0;
            List<int> free = new List<int>(MaxMachines);
            for (int i = 0; i < MaxJobs; i++) {
                List<int> toFree = new List<int>();
                for (int j = 0; j < MaxMachines; j++)
                    if (_calendar[j, i] != null)
                        free.Remove(j);
                    else if(!free.Contains(j))
                        toFree.Add(j);
                    
                withoutClean++;
                if (free.Count < machines) {
                    if (free.Count + toFree.Count >= machines) {
                        withoutClean = 1;
                        free.AddRange(toFree);
                    } else {
                        withoutClean = 0;
                        free.Clear();
                        for (int k = 0; k < MaxMachines; k++)
                            free.Add(k);
                    }
                }
                if (withoutClean == duration)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Posklada stroje do planu
        /// </summary>
        /// <param name="t">cas na umisteni</param>
        /// <param name="free">volne stroje</param>
        /// <param name="job">job na umisteni</param>
        private void AddToCalendar(int t, List<int> free, Job job)
        {
            for (int i = 0; i < job.Time; i++) {
                int added = 0;
                free.ForEach(machine => {
                    if (added < job.Machines) {
                        if(_calendar[machine, i + t] != null)
                            throw new ArgumentException("Interni chyba planovace");
                        _calendar[machine, i + t] = job;
                        added++;
                    }
                });
            }
            job.Id = JobTools.NextId();
        }
    }
}