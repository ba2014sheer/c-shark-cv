﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Planner
{
    /// <summary>
    /// Generator ID
    /// </summary>
    public class JobTools
    {
        /// <summary>
        /// Dalsi ID
        /// </summary>
        private static char _next = 'A';

        /// <summary>
        /// Generuje ID
        /// </summary>
        /// <returns>Dalsi ID</returns>
        public static char NextId()
        {
            if (_next > 'Z' && _next < 'a')
                _next = 'a';
            return _next++;
        }

        public static List<Job> LoadJobs(string path, int fileNumber)
        {
            List<Job> list = new List<Job>();
            int i = 1;
            using (StreamReader reader = new StreamReader(path)) {
                string line;
                while ((line = reader.ReadLine()) != null){
                    var parts = line.Split(',');

                    if (parts.Length < 5)
                    {
                        Console.WriteLine($"Job na radku se nepovedlo dekodovat. Pocet informaci je mensi jak 5 ({i})");
                    }
                    else if (!parts[0].Equals("JOB"))
                    {
                        Console.WriteLine($"Job na radku se nepovedlo dekodovat. Chybi identifikator JOB ({i})");
                    }
                    else
                    {
                        int crateDate, machines, time;
                        bool add = true;
                        if (Int32.TryParse(parts[1], out crateDate) == false)
                        {
                            add = false;
                            Console.WriteLine($"Job na radku se nepovedlo dekodovat. Neplatne casove razitko ({i})");
                        }

                        if (Int32.TryParse(parts[2], out machines) == false)
                        {
                            add = false;
                            Console.WriteLine($"Job na radku se nepovedlo dekodovat. Neplatny potrebny pocet stroju ({i})");
                        }

                        if (Int32.TryParse(parts[3], out time) == false)
                        {
                            add = false;
                            Console.WriteLine($"Job na radku se nepovedlo dekodovat. Neplatny cas stroju ({i})");
                        }
                        if (add)
                            if (machines > Planner.MaxMachines)
                                Console.Write($"Job na radku se nepovedlo dekodovat. Pocet stroju je vetsi nez {Planner.MaxMachines} ({i})");
                            else if (time > Planner.MaxJobs )
                                Console.Write($"Job na radku se nepovedlo dekodovat. Cas je vetsi nez {Planner.MaxJobs} ({i})");
                            else
                                list.Add(new Job(crateDate, machines, time, fileNumber));
                    }
                }
                ++i;
            }
            return list;
        }
    }
    /// <summary>
    /// Prace
    /// </summary>
    public class Job
    {
        /// <summary>
        /// Casove razitko
        /// </summary>
        public int CreateDate { get; }

        /// <summary>
        /// Pocet potrebnych stroju
        /// </summary>
        public int Machines { get; }

        /// <summary>
        /// Cas trvani ve slotech
        /// </summary>
        public int Time { get; }
        
        /// <summary>
        /// Nejaka hodnota
        /// </summary>
        public int U { get; }
        
        /// <summary>
        /// Identifikator
        /// </summary>
        public char Id { get; set; }

        /// <summary>
        /// Vytvori job
        /// </summary>
        /// <param name="date">datum vytvoreni</param>
        /// <param name="machines">potrebny pocet stroju</param>
        /// <param name="time">cas ve slotech</param>
        /// <param name="fileNumber"></param>
        public Job(int date, int machines, int time, int fileNumber)
        {
            CreateDate = date;
            Machines = machines;
            Time = time;
            U = fileNumber;
        }

        public override string ToString()
        {
            return $"{Id} (u:{U}, shares:{Machines}, duration:{Time}, createdat:{CreateDate})";
        }
    }
}