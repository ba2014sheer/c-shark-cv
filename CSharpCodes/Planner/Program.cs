﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Planner
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            while (LoadPlann())
                Console.WriteLine("Přejete si zadal další plán?");
        }
        
        /// <summary>
        /// Spusti nacteni planu
        /// <returns>true - dalsi kolo</returns>
        /// </summary>
        private static bool LoadPlann() {
            List<Job> jobs =new List<Job>();
            //Zjisti slozku se soubory
            Console.Write("Zadejte adresar se soubory (0 pro ukonceni):");
            string path = Console.ReadLine();
            if (path != null && "0".Equals(path.Trim()))
                return false;
            if (!Directory.Exists(path)) {
                Console.WriteLine("Cesta k souboru neexistuje!");
                return true;
            }
            try
            {
                var files = Directory.GetFiles(path);
                int i = 0;
                foreach (var file in files)
                {
                    try
                    {
                        jobs.AddRange(JobTools.LoadJobs(file, i++));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Nepodarilo se zpracovat soubor {file}! Vstupne vystupni chyba {e.Message}");
                    }
                }

                Planner planner = new Planner(jobs);
                Console.WriteLine(planner);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Vstupne vystupni chyba! {ex.Message}");
            }
            return true;
        }
    }
}