﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Graph
{
    /// <summary>
    /// Trida predstavujici graf
    /// </summary>
    public class Graph
    {
        /// <summary>
        /// Reprezentuje hranu grafu
        /// </summary>
        public class Node
        {
            /// <summary>
            /// Id hrany
            /// </summary>
            public int Id { get; private set; }

            /// <summary>
            /// Predek uzlu
            /// </summary>
            public LinkedList<Node> Ancestor { get; private set; }

            /// <summary>
            /// Potomci uzlu
            /// </summary>
            public LinkedList<Node> Childs { get; private set; }

            /// <summary>
            /// Inicializuje hranu
            /// </summary>
            /// <param name="id">id hrany</param>
            /// <param name="label"></param>
            public Node(int id, string label)
            {
                Id = id;
                Childs = new LinkedList<Node>();
                Ancestor = new LinkedList<Node>();
                Label = label;
            }

            /// <summary>
            /// Label uzlu
            /// </summary>
            public string Label { get; private set; }

            public override string ToString()
            {
                StringBuilder builder = new StringBuilder();
                builder.Append($"Node = {Id}, Ancestor = None Childs = [");
                foreach (var child in Childs)
                    builder.Append($"{child.Id} ");
                builder.Append("]");
                return builder.ToString();
            }
        }
        
        /// <summary>
        /// Vsechny hrany grafu
        /// </summary>
        private Dictionary<int, Node> _nodes = new Dictionary<int, Node>();
        
        /// <summary>
        /// labely k id
        /// </summary>
        private Dictionary<string, int> _labels = new Dictionary<string, int>();
        
        /// <summary>
        /// Uzli s referenci sami na sebe
        /// </summary>
        public LinkedList<Node> SelfNodes { get; private set; }

        /// <summary>
        /// Inicializuje graf
        /// </summary>
        /// <param name="path"></param>
        public Graph(string path)
        {
            SelfNodes = new LinkedList<Node>();
            ReadFromFile(path);
        }
        

        /// <summary>
        /// Precte graf ze souboru
        /// </summary>
        /// <param name="path">Cesta k souboru</param>
        /// <returns>true - soubor precten, false - soubor neprecten</returns>
        /// <exception cref="ArgumentException">Chyba pri cteni souboru</exception>
        public void ReadFromFile(string path)
        {
            StringBuilder builder = new StringBuilder();
            string line;

            Dictionary<int, Node> nodes = new Dictionary<int, Node>();
            Dictionary<string, int> labeles = new Dictionary<string, int>();
            LinkedList<Node> singletons = new LinkedList<Node>();

            bool header = true;
            using (StreamReader reader = new StreamReader(path))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    line = line.Trim().Replace("\t", " ");
                    builder.Append(line);

                    var builderString = builder.ToString();
                    if (builderString.Contains("label \"") && header)
                    {
                        header = false;
                        builder.Clear();
                        continue;
                    }

                    if (builderString.Contains("node[id ") && builderString.Contains("label \""))
                    {
                        string[] numbers = builderString.Split(new[] {"node[id "}, StringSplitOptions.None);
                        string[] labels = builderString.Split(new[] {"label \""}, StringSplitOptions.None);
                        if (numbers.Length > 1 && labels.Length > 1 && numbers.Length == labels.Length)
                        {
                            ReadNodes(numbers, labels, nodes, labeles);
                            builder.Clear();
                        }
                    }

                    if (builderString.Contains("edge[source ") && builderString.Contains("target "))
                    {
                        string[] sources = builderString.Split(new[] {"edge[source "}, StringSplitOptions.None);
                        string[] targets = builderString.Split(new[] {"target "}, StringSplitOptions.None);
                        if (sources.Length > 1 && targets.Length > 1 && sources.Length == targets.Length)
                        {
                            ReadEdges(sources, targets, nodes, singletons);
                            builder.Clear();
                        }
                    }
                }
            }

            _nodes = nodes;
            _labels = labeles;
            SelfNodes = singletons;
        }

        /// <summary>
        /// Přečte vrchol a napojí ho na Node
        /// </summary>
        /// <param name="sources">Zdrojový vrchol</param>
        /// <param name="targets">Cílový vrchol</param>
        /// <param name="nodesTable">Tabulka ulzů</param>
        /// <param name="selfNodes">Uzly s ukazatelem sama na sebe</param>
        /// <returns>Počet přečtených vrcholů</returns>
        /// <exception cref="InvalidDataException">Grah neobsahuje uzel</exception>
        private void ReadEdges(string[] sources, string[] targets, Dictionary<int, Node> nodesTable, LinkedList<Node> selfNodes) {
            for (int i = 1; i < sources.Length; i++) {
                var source = ReadNumber(sources[i]);
                var target = ReadNumber(targets[i]);
                if (!nodesTable.ContainsKey(source))
                    throw new InvalidDataException($"Grah neobsahuje uzel s id {source}!");
                if (!nodesTable.ContainsKey(target))
                    throw new InvalidDataException($"Grah neobsahuje uzel s id {target}!");
                Node sourceN = nodesTable[source];
                Node targetN = nodesTable[target];
                if (sourceN != null && targetN != null) {
                    sourceN.Childs.AddLast(targetN);

                    if (source != target && !targetN.Ancestor.Contains(sourceN))
                        targetN.Ancestor.AddLast(sourceN);
                    if (source == target)
                        selfNodes.AddLast(sourceN);
                }
            }
        }

        /// <summary>
        /// Precte uzel
        /// </summary>
        /// <param name="possibleNodes">Nody ve stringu</param>
        /// <param name="labels"></param>
        /// <param name="nodesTable">Tabulka pro ulozeni</param>
        /// <param name="labeles"></param>
        /// <returns>pocet uspesne prectenych nodu</returns>
        /// <exception cref="InvalidDataException">Duplicitni klic</exception>
        private void ReadNodes(string[] possibleNodes, string[] labels, Dictionary<int, Node> nodesTable,
            Dictionary<string, int> labeles) {
            for (int i = 1; i < possibleNodes.Length; i++) {
                var number = possibleNodes[i];
                var label = labels[i].Split('\"')[0];
                int output = ReadNumber(number);
                if (nodesTable.ContainsKey(output))
                    throw new InvalidDataException("Soubor obsahuje duplicitni klic");
                var node = new Node(output, label);
                nodesTable.Add(output, node);
                labeles.Add(label, output);
            }
        }

        /// <summary>
        /// Přečte číslo
        /// </summary>
        /// <param name="number">string s číslem</param>
        /// <returns>číslo</returns>
        private int ReadNumber(string number) {
            string result = "";
            foreach (char c in number) {
                if (Char.IsDigit(c))
                    result += c;
                else
                    break;
            }
            return Int32.Parse(result);
        }

        /// <summary>
        /// Metoda vrati nody, ktere nejsou dosazitelne
        /// </summary>
        /// <param name="id">Od u ktereho se ma hledat</param>
        /// <returns>Vrati nedosazitelne nody</returns>
        public List<List<Node>> FindUnreachable(string id) {
            List<List<Node>> unreachable = new List<List<Node>>();
            if(!_labels.ContainsKey(id) || !_nodes.ContainsKey(_labels[id]))
                throw new ArgumentException("Neplatny nazev");
            Node myNode = _nodes[_labels[id]];

            List<Node> reachable = GetReachable(myNode);
            List<Node> unreachableAnc = GetMyAncestorsAndChilds(myNode, reachable, false);
            reachable.Sort((node, node1) => { return node.Id - node1.Id; });
            unreachableAnc.Remove(myNode);
            unreachableAnc.Sort((node, node1) => node.Id - node1.Id);
            
            unreachable.Add(unreachableAnc);
            
            foreach (var nodesValue in _nodes.Values)
            {
                if(nodesValue == myNode || reachable.Contains(nodesValue))
                    continue;

                bool contains = false;
                foreach (var list in unreachable) {
                    if (list.Contains(nodesValue)) {
                        contains = true;
                    }
                }
                if (!contains) {
                    var toAdd = GetMyAncestorsAndChilds(nodesValue, reachable, true);
                    toAdd.Sort((node, node1) => node.Id - node1.Id);
                    unreachable.Add(toAdd);
                }
            }
            
            return unreachable;
        }

        /// <summary>
        /// Vrati vsechny dosazitelne prvky
        /// </summary>
        /// <param name="nodeFind">Node u ktereho se budou hledat potomci</param>
        /// <returns>Potomci nodu</returns>
        private List<Node> GetReachable(Node nodeFind) {
            Dictionary<int, Node> reachable = new Dictionary<int, Node>();
            Queue<Node> next = new Queue<Node>();

            next.Enqueue(nodeFind);
            while (next.Count > 0)
            {
                Node node = next.Dequeue();
                if (!reachable.ContainsKey(node.Id))
                {
                    reachable.Add(node.Id, node);
                    foreach (var nodeChild in node.Childs)
                        if (nodeChild != node)
                            next.Enqueue(nodeChild);
                }
            }
            return reachable.Values.Cast<Node>().ToList();
        }

        /// <summary>
        /// Vrati skupinu Nodu
        /// </summary>
        /// <param name="nodeFind">Prvek, ktery se ma hledat</param>
        /// <param name="except">Vyjimky</param>
        /// <param name="childs">Maji se zahrnout potomci prvniho nodu?</param>
        /// <returns>Skupinu nodu</returns>
        private List<Node> GetMyAncestorsAndChilds(Node nodeFind, List<Node> except, bool childs) {
            Dictionary<int, Node> all = new Dictionary<int, Node>();
            Queue<Node> next = new Queue<Node>();
            
            all.Add(nodeFind.Id, nodeFind);

            foreach (var node in nodeFind.Ancestor)
                next.Enqueue(node);
            
            if(childs)
                foreach (var node in nodeFind.Childs)
                    next.Enqueue(node);
            
            while (next.Count > 0)
            {
                Node node = next.Dequeue();
                if (!all.ContainsKey(node.Id) && !except.Contains(node))
                {
                    all.Add(node.Id, node);
                    foreach (var nodeA in node.Ancestor)
                        next.Enqueue(nodeA);
                    foreach (var nodeCh in node.Childs)
                        next.Enqueue(nodeCh);
                }
            }
            
            return all.Values.ToList();
        }
    }
}