﻿using System;
using System.Collections.Generic;

namespace Graph
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //ReadGraph(@"vstup2.dat");
            ReadGraph(@"vstup.dat");
        }

        static void ReadGraph(string path)
        {
            Graph graph;
            try
            {
                graph = new Graph(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Chyba při sestrojení grafu: " + ex);
                return;
            }

            while (true)
            {
                bool reading = true;
                Console.WriteLine();
                Console.Write("Zadejte nazev vrcholu: ");
                var id = Console.ReadLine();

                List<List<Graph.Node>> result;
                try
                {
                    result = graph.FindUnreachable(id);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine("Naplatny nazev.");
                    continue;
                }

                Console.WriteLine($"Algoritmus (1) s počátečním vrcholem {id} identifikuje následující skupiny objektů jako nedosažitelné:");
                result.ForEach(ints => { if (ints.Count > 0)
                    {
                        Console.Write("=>");
                        ints.ForEach(node => { Console.Write($" {node.Label} "); });
                        Console.WriteLine();
                    }
                });
                //Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("\nAlgoritmus (2) identifikuje následující reference sama na sebe:");
                foreach (var node in graph.SelfNodes)
                    Console.WriteLine($" {node.Label} ");
            }
        }
    }
}