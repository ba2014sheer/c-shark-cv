﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GenericExercise
{
    /// <summary>
    /// Hashovaci tabulka FEI
    /// </summary>
    /// <typeparam name="K">genericky typ pro klic</typeparam>
    /// <typeparam name="V">genericky typ pro hodnotu</typeparam>
    public class MinMaxHashTable<K, V> where K : IComparable where V : IComparable {
        /// <summary>
        /// Obsah tabulky
        /// </summary>
        private Node[] fields;

        private K maximumKey;
        /// <summary>
        /// Maximalni klic
        /// </summary>
        public K Maximum { 
            get{
                if (Count == 0)
                    throw new InvalidOperationException("HashTable is empty");
                return maximumKey;
            }
            set{
                maximumKey = value;
            }
        }

        private K minimumKey;
        /// <summary>
        /// Minimalni klic
        /// </summary>
        public K Minimum{
            get {
                if (Count == 0)
                    throw new InvalidOperationException("HashTable is empty");
                return minimumKey;
            }
            set {
                minimumKey = value;
            }
        }

        /// <summary>
        /// Delka kolekce
        /// </summary>
        public int Count;

        public class Node
        {
            /// <summary>
            /// Zretezene hodnoty
            /// </summary>
            public Node Next { get; set; }

            /// <summary>
            /// Klic
            /// </summary>
            public K Key { get; private set; }

            /// <summary>
            /// Hodnota
            /// </summary>
            public V Val { get; private set; }

            /// <summary>
            /// Inicializuje objekt
            /// </summary>
            /// <param name="key">klic</param>
            /// <param name="val">hodnota</param>
            public Node(K key, V val)
            {
                this.Key = key;
                this.Val = val;
            }
        }

        /// <summary>
        /// Inicializuje tabulku na vychozi velikost 20
        /// </summary>
        public MinMaxHashTable()
        {
            fields = new Node[20];
        }

        /// <summary>
        /// Inicializuje tabulku
        /// </summary>
        /// <param name="size">Vychozi velikost</param>
        public MinMaxHashTable(int capacity)
        {
            fields = new Node[capacity];
        }

        /// <summary>
        /// Zkontroluje klic
        /// </summary>
        /// <param name="key">klic ke kontrole</param>
        /// <param name="node">Svazek ke kontrole</param>
        /// <returns></returns>
        private bool CheckKey(K key, Node node) {
            while (node != null)
            {
                if (node.Key.Equals(key))
                    return false;
                node = node.Next;
            }
            return true;
        }

        /// <summary>
        /// Prida klic do hashmapy
        /// </summary>
        /// <param name="key">klic</param>
        /// <param name="val">hodnota</param>
        public void Add(K key, V val)
        {
            int index = Math.Abs(key.GetHashCode() % fields.Length);
            Node node = new Node(key, val);

            if (fields[index] == null)
                fields[index] = node;
            else if (CheckKey(key, fields[index])){
                node.Next = fields[index];
                fields[index] = node;
            }
            else {
                throw new ArgumentException("Duplicitni klic byl pridan.");
            }
            

            if (minimumKey.Equals(default(K)) || Minimum.GetHashCode() > key.GetHashCode())
                Minimum = key;
            if (maximumKey.Equals(default(K))  || Maximum.GetHashCode() < key.GetHashCode())
                Maximum = key;
            Count++;
        }

        /// <summary>
        /// Zjisti jestli tabulka obsahuje klic
        /// </summary>
        /// <param name="key">Vrací true, pokud je klíč přítomen v tabulce.</param>
        /// <returns>true/false pritomen nepritomen</returns>
        public bool Contains(K key)
        {
            int index = Math.Abs(key.GetHashCode() % fields.Length);
            if (fields[index] == null)
                return false;
            Node next = fields[index];
            while (next != null)
            {
                if (next.Key.Equals(key))
                    return true;
                next = next.Next;
            }

            return false;
        }

        /// <summary>
        /// Vrati hodnotu daneho klice
        /// </summary>
        /// <param name="key">klic k hodnote</param>
        /// <returns>hodnota</returns>
        public V Get(K key)
        {
            int index = Math.Abs(key.GetHashCode() % fields.Length);
            if (fields[index] == null)
                throw new KeyNotFoundException();
            Node next = fields[index];
            while (next != null)
            {
                if (next.Key.Equals(key))
                    return next.Val;
                next = next.Next;
            }

            throw new KeyNotFoundException();
        }

        /// <summary>
        /// Odebere klíč z tabulky a vrátí jemu přiřazenou hodnotu.
        /// </summary>
        /// <param name="key">Klic</param>
        /// <returns>Jeho hodnota</returns>
        public V Remove(K key)
        {
            int index = Math.Abs(key.GetHashCode() % fields.Length);

            if (fields[index] == null)
                throw new KeyNotFoundException();

            if (fields[index].Key.Equals(key))
            {
                V val = fields[index].Val;
                fields[index] = fields[index].Next;
                Count--;
                return val;
            }

            Node next = fields[index];
            while (next.Next != null && next.Next.Key.Equals(key))
                next = next.Next;

            if (next.Next == null)
                throw new KeyNotFoundException();

            V val2 = next.Next.Val;
            next.Next = next.Next.Next;
            Count--;
            return val2;
        }

        /// <summary>
        /// Vrátí enumerovatelný objekt obsahující všechny prvky z hash tabulky
        /// </summary>
        /// <param name="min">Minimální hodnota klíce</param>
        /// <param name="max">Maximální hodnota klice</param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<K, V>> Range(K min, K max)
        {
            int imin = Math.Abs(min.GetHashCode() % fields.Length);
            int imax = Math.Abs(max.GetHashCode() % fields.Length);

            for (int i = imin; i <= imax; i++)
            {
                Node next = fields[i];
                while (next != null)
                {
                    if (next.Key.GetHashCode() >= min.GetHashCode() && next.Key.GetHashCode() <= max.GetHashCode())
                        yield return new KeyValuePair<K, V>(next.Key, next.Val);
                    next = next.Next;
                }
            }
        }

        /// <summary>
        /// Vrátí enumerovatelný objekt obsahující všechny prvky z hash tabulky
        /// </summary>
        /// <param name="min">Minimální hodnota klíce</param>
        /// <param name="max">Maximální hodnota klice</param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<K, V>> SortedRange(K min, K max)
        {
            return Range(min, max).OrderBy(pair => pair.Key.GetHashCode());
        }

        /// <summary>
        /// Vrátí enumerovatelný objekt obsahující všechny prvky z hash tabulky
        /// </summary>
        /// <param name="min">Minimální hodnota klíce</param>
        /// <param name="max">Maximální hodnota klice</param>
        /// <returns>Indexer</returns>
        public IEnumerable<KeyValuePair<K, V>> this[K min, K max]
        {
            get { return SortedRange(min, max); }
        }
    }
}