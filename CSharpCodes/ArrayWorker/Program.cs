﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fei.BaseLib;

namespace ArrayWorker
{
    /// <summary>
    /// Třída realizující jednoduchý program pro práci s polem
    /// </summary>
    class Program
    {
        /// <summary>
        /// Hlavní pole aplikace
        /// </summary>
        private static int[] array = new int[0];

        /// <summary>
        /// Hlavní metoda Main. Zde začíná program.
        /// </summary>
        /// <param name="args">Vstupní parametry příkazové řádky</param>
        static void Main(string[] args)
        {
            
            bool cont = false;
            int item = 0;
            do
            {
                PrintMenu();
                item = ReadItem();
                cont = item > 0 && item < 7;
                if (cont)
                    RunOperation(item);
            } while (cont);
        }

        /// <summary>
        /// Přidá prvek do pole
        /// </summary>
        static void Add() {
            int newValue = 0;
            bool end = false;
            Console.WriteLine("Zadavejte tolik hodnot kolik chcete. Zadanim neciselne hodnoty skonci zadavani...");
            do {
                try
                {
                    newValue = Reading.ReadInt("Zadejte hodnotu");
                    Array.Resize(ref array, array.Length + 1);
                    array[array.Length - 1] = newValue;
                }
                catch (InvalidCastException ex)
                {
                    end = true;
                }
            } while (!end);
        }

        /// <summary>
        /// Najde nejbližší výskyt zadaného čísla. Vstup čísla si metoda řídí sama
        /// </summary>
        static void FindLowest() {
            int find = 0;
            try{
                find = Reading.ReadInt("Zadejte hledanou hodnotu");
            }
            catch (InvalidCastException ex){
                Console.WriteLine("Zadaná hodnota není celé číslo!");
                return;
            }

            for (var i = 0; i < array.Length; ++i) {
                if (array[i] == find) {
                    Console.WriteLine($"Prvej nalezen na indexu {i}!");
                    return;
                }
            }
            Console.WriteLine("Prvek nenalezen.");
        }

        /// <summary>
        /// Najde poslední výskyt zadaného čísla. Vstup čísla si metoda řídí sama
        /// </summary>
        static void FindHightes(){
            int find = 0;
            try{
                find = Reading.ReadInt("Zadejte hledanou hodnotu");
            }
            catch (InvalidCastException ex){
                Console.WriteLine("Zadaná hodnota není celé číslo!");
                return;
            }

            for (var i = array.Length - 1; i >= 0 ; --i){
                if (array[i] == find){
                    Console.WriteLine($"Prvej nalezen na indexu {i}!");
                    return;
                }
            }
            Console.WriteLine("Prvek nenalezen.");
        }

        /// <summary>
        /// Najde nejmenší číslo z pole
        /// </summary>
        static void Min() {
            int min = Int32.MaxValue;
            foreach (var i in array) {
                if (i < min)
                    min = i;
            }
            if(array.Length != 0)
                Console.WriteLine($"Nejmenší prvek je {min}");
            else
                Console.WriteLine($"Pole neobsahuje žádné hodnoty");
        }

        /// <summary>
        /// Seřadí pole
        /// </summary>
        /// <param name="direction">Typ řazení. True = vzestupně, False = sestupně</param>
        static void Sort(bool direction) {
            int temp;
            for (int j = 0; j <= array.Length - 2; j++)
            {
                for (int i = 0; i <= array.Length - 2; i++)
                {
                    bool swap = false;
                    if (direction)
                        swap = array[i] > array[i + 1];
                    else
                        swap = array[i] < array[i + 1];
                    if (swap) {
                        temp = array[i + 1];
                        array[i + 1] = array[i];
                        array[i] = temp;
                    }
                }
            }
        }

        /// <summary>
        /// Vypíše prvky do konzole
        /// </summary>
        static void Print() {
            for (var i = 0; i < array.Length; ++i) {
                Console.Write($"{array[i]} ");
            }
            Console.WriteLine("");
        }

        /// <summary>
        /// Provede danou operaci
        /// </summary>
        /// <param name="item">Druh operace</param>
        static void RunOperation(int item) {
            switch (item) {
                case 0:
                    PrintMenu();
                    break;
                case 1:
                    Add();
                    break;
                case 2:
                    Print();
                    break;
                case 3:
                    Sort(true);
                    break;
                case 4:
                    Sort(false);
                    break;
                case 5:
                    Min();
                    break;
                case 6:
                    FindLowest();
                    break;
                case 7:
                    FindHightes();
                    break;
            }
        }

        /// <summary>
        /// Přečte a zvaliduje vstup
        /// </summary>
        /// <returns>Validní vstup ve formátu celé číslo</returns>
        static int ReadItem() {
            int item = 0;
            bool end = false;
            do{
                try{
                    item = Reading.ReadInt("Zadejte volbu");
                    end = true;
                }
                catch (InvalidCastException ex){
                    Console.WriteLine("Neplatný vstup, zkuzte to znovu!");
                }
            } while (!end);
            return item;
        }

        /// <summary>
        /// Vypíše menu aplikace.
        /// </summary>
        static void PrintMenu()
        {
            Console.WriteLine("\n------------------------------------------------");
            Console.WriteLine("0 - Vypis menu");
            Console.WriteLine("1 - Zadat prvek");
            Console.WriteLine("2 - Vypis pole");
            Console.WriteLine("3 - Setřiď pole vzestupně");
            Console.WriteLine("4 - Setřiď pole sestupně");
            Console.WriteLine("5 - Zobraz minimální prvek");
            Console.WriteLine("6 - Najdi číslo (první)");
            Console.WriteLine("7 - Najdi číslo (poslední)");
            Console.WriteLine("Jiná volba - Ukonči program");
            Console.WriteLine("\n------------------------------------------------");
        }
    }
}
