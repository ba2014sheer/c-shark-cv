﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Ukol5
{
    class Program
    {
        static void Main(string[] args)
        {
            string line = "";
            string lat;
            string lon;

            Console.Write("Enter place: ");
            line = Console.ReadLine().ToUpper();
        
            //Getting city
            System.Net.WebClient web2 = new System.Net.WebClient();
            string response2 = "";

            try{
                response2 = web2.DownloadString($"https://geocode.xyz/{line}?geoit=xml");
            }
            catch (Exception ex) {
                Console.WriteLine("Connection to Geocode failed.");
                Console.ReadKey();
                return;
            }

            System.Xml.XmlDocument xml2 = new System.Xml.XmlDocument();
            xml2.LoadXml(response2);
            XmlNamespaceManager manager2 = new XmlNamespaceManager(xml2.NameTable);

            XmlNode latN = xml2.SelectSingleNode("//latt", manager2);
            XmlNode lonN = xml2.SelectSingleNode("//longt", manager2);

            if (latN == null || lonN == null) {
                Console.WriteLine("Place wasnt found!");
                Console.ReadKey();
                return;
            }

            lat = (latN.InnerText);
            lon = (lonN.InnerText);

            //Calling API
            System.Net.WebClient web = new System.Net.WebClient();
            var response = "";

            try{
                response = web.DownloadString($"https://api.met.no/weatherapi/locationforecast/1.9/?lat={lat}&lon={lon}&msl=70");
            }
            catch (Exception ex) {
                Console.WriteLine("Connection to API failed.");
                Console.ReadKey();
                return;
            }

            //Getting XML
            System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
            xml.LoadXml(response);
            XmlNamespaceManager manager = new XmlNamespaceManager(xml.NameTable);

            //Parsing XMl
            XmlNode tem = xml.SelectSingleNode("//temperature", manager);
            XmlNode wind = xml.SelectSingleNode("//windSpeed", manager);
            XmlNode press = xml.SelectSingleNode("//pressure", manager);

            if (tem == null || wind == null || press == null) {
                Console.WriteLine("Getting weather failed.");
                Console.ReadKey();
                return;
            }

            //Gettong values
            Console.WriteLine($"Temperature: {tem.Attributes["value"].Value} C \nWind speed: {wind.Attributes["mps"].Value} m/s \nPressure: { press.Attributes["value"].Value} mPA");
            Console.ReadKey();
        }
    }
}
