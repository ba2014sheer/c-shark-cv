﻿﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCXReader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            LoadPCX("Resources/marbles.pcx");
        }

        private Bitmap LoadPCX(string filename)
        {
            BinaryReader reader = new BinaryReader(File.Open(filename, FileMode.Open));
            //Read header
            var Identifer = reader.ReadByte();
            var Version = reader.ReadByte();
            var Encoding = reader.ReadByte();
            var BitsPerPixel = reader.ReadByte();
            var XStart = reader.ReadUInt16();
            var YStart = reader.ReadUInt16();
            var XEnd = reader.ReadUInt16();
            var YEnd = reader.ReadUInt16();
            var HorzRes = reader.ReadUInt16();
            var VertRes = reader.ReadUInt16();
            var Pal = reader.ReadBytes(48);
            var Reserved1 = reader.ReadByte();
            var NumBitPlanes = reader.ReadByte();
            var BytesPerLine = reader.ReadUInt16();
            var PaletteType = reader.ReadUInt16();
            var HorzScreenSize = reader.ReadUInt16();
            var VertScreenSize = reader.ReadUInt16();
            var Reserved2 = reader.ReadBytes(54);

            //Counting additional information
            var MaxNumberOfColors = (1L << (BitsPerPixel * NumBitPlanes));
            var ImageWidth = XEnd - XStart + 1;
            var ImageHeight = YEnd - YStart + 1;
            var ScanLineLength = NumBitPlanes * BytesPerLine;
            var LinePaddingSize = ((BytesPerLine * NumBitPlanes) * (8 / BitsPerPixel)) - ((XEnd - XStart) + 1);
            byte runcount = 0;
            var total = 0;
            byte runvalue = 0;
            var index = 0;

            List<byte[]> Data = new List<byte[]>();
            for (int i = 0; i < ImageHeight-1; i++) {
                runcount = 0;
                runvalue = 0;
                index = 0;
                total = 0;
                //Decoding data
                byte[] DecodeBuffer = new byte[ScanLineLength];
                do
                {
                    for (total += runcount; runcount != 0 && index < DecodeBuffer.Length; runcount--, index++)
                    {
                        DecodeBuffer[index] = runvalue;
                    }

                    if (runcount != 0)
                    {
                        total -= runcount;
                        break;
                    }

                    byte bajt = reader.ReadByte();
                    if ((bajt & 0xC0) == 0xC0)
                    {
                        runcount = (byte) (bajt & 0x3F);
                        runvalue = reader.ReadByte();
                    }
                    else
                    {
                        runcount = 1;
                        runvalue = bajt;
                    }
                } while (index < DecodeBuffer.Length);

                //Encoded data
                List<byte> EncodedData = new List<byte>();
                index = 0;
                var scanIndex = 0;
                while (index < DecodeBuffer.Length)
                {
                    for (runcount = 1, runvalue = DecodeBuffer[index];
                        index + runcount < DecodeBuffer.Length && runvalue == DecodeBuffer[index + runcount] &&
                        runcount < 63;)
                    {
                        runcount++;
                    }

                    if (runcount > 1)
                    {
                        EncodedData.Add((byte) (runcount | 0xC0));
                        EncodedData.Add(runvalue);
                    }
                    else
                    {
                        if (DecodeBuffer[index] < 64) /* Value is 0 to 63   */
                        {
                            EncodedData.Add(runvalue);
                        }
                        else /* Value is 64 to 255 */
                        {
                            EncodedData.Add((byte) (runcount | 0xC0));
                            EncodedData.Add(runvalue);
                        }
                    }

                    index += runcount;
                }
                Data.Add(DecodeBuffer);
            }

            var cheek = reader.ReadByte();

            Bitmap bitmap = new Bitmap(ImageWidth-1, ImageHeight-1);
            int next = 0;
            for (int k = 0; k < Data.Count-1; k++)
            {
                byte[] line = Data[k];
                var nextB = 0;
                for (int j = 0; j < line.Length-1; j++)
                {
                    bitmap.SetPixel(j, k, Color.FromArgb(line[nextB++], line[nextB++], line[nextB++]));
                }
            }

            pictureBox.Image = bitmap;

            Console.WriteLine(NumBitPlanes);
            reader.Close();

            return null;
        }
    }
}