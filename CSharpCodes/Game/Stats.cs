﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Stats
    {
        public delegate void UpdateStatsEventHandler(object sender, EventArgs e);
        public int Correct { get; private set; }
        public int Missed { get; private set; }
        public int Accuary { get; private set; }
        public event UpdateStatsEventHandler UpdateStats;
        private void OnUpdateStats() {
            UpdateStatsEventHandler handler = UpdateStats;
            if (handler != null) {
                handler(this, new EventArgs());  
            }
        }

        public void Update(bool correctKey) {
            if (correctKey)
                Correct += 1;
            else 
                Missed += 1;
            if(Missed + Correct != 0)
                Accuary = (int)(((double)Correct /(Missed + Correct))*100);
            OnUpdateStats();
        }
    }
}
