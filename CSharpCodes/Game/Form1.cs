﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class Form1 : Form
    {
        private readonly Random random = new Random();

        private readonly Stats stats = new Stats();

        private List<string> words;

        private string selected;

        public Form1()
        {
            InitializeComponent();

            words = readFile();

            this.stats.UpdateStats += delegate (object sender, EventArgs e) {
                Stats stats = (Stats)sender;
                this.correctLabel.Text = "Correct: " + stats.Correct.ToString();
                this.missedLabel.Text = "Missed: " + stats.Missed.ToString();
                this.accurancyLabel.Text = "Accuarry: " + stats.Accuary.ToString() + "%";
            };
            timer.Tick += delegate (object sender, EventArgs e) {
                this.gameListBox.Items.Add((Keys)random.Next(65, 90));
                this.gameListBox.Refresh();
                if (this.gameListBox.Items.Count > 6) {
                    timer.Stop();
                    this.gameListBox.Items.Clear();
                    this.gameListBox.Items.Add("Game Over!");
                }
            };
            gameListBox.KeyDown += delegate (object sender, KeyEventArgs e){


                bool correct = false;
                if (gameListBox.Items.Contains(e.KeyCode))
                {
                    gameListBox.Items.Remove(e.KeyCode);
                    gameListBox.Refresh();
                    correct = true;
                }
                this.stats.Update(correct);

                if (timer.Interval > 400) {
                    timer.Interval -= 60;
                } else if (timer.Interval > 250){
                    timer.Interval -= 15;
                } else if (timer.Interval > 150){
                    timer.Interval -= 8;
                }
                int value = 800 - timer.Interval;

                if (value > 0 && value <= 800) {
                    difficultProgressBar.Value = value;
                }
                    
            };
        }

        private List<String> readFile() {
            List<String> words = new List<string>();
            string line = "";
            System.IO.StreamReader file =new System.IO.StreamReader("words.txt");
            while ((line = file.ReadLine()) != null)
            {
                words.Add(line);
            }

            file.Close();

            return words;
        }
    }
}
