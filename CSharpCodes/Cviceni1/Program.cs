﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cviceni1
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "Josef Novák";
            var street = "Jindrišská 16";
            var psc = "111 50";
            var city = "Praha 1";

            Console.WriteLine($"{name}");
            Console.Write($"{street}\n{psc}, {city}");
            Console.ReadKey();
        }
    }
}
